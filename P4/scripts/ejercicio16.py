# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import sys

def load_vocabulary(file):
	vocab = []
	with open(file, 'r') as f:
		spl = ['','']
		while len(spl) == 2:	
			spl = f.readline().split()
			if len(spl) == 2:
				vocab.append(spl[1])
			
	return vocab
	

def get_words_ids(sample_number, file):
	data = pd.read_csv(file, header=None)
	X = data.iloc[:,:-1].values
	
	return np.nonzero(X[sample_number] == 1)[0]
			
	
def get_words(sample_number):
	# Get vocabulary
	vocab = load_vocabulary('etiquetasYVocabulario/vocab.txt')

	word_ids = get_words_ids(sample_number, 'basesDatosPr4IMC/csv/test_spam.csv')
	
	for i in word_ids:
		sys.stdout.write('{}, '.format(vocab[i]))
		

words = get_words(881)
	