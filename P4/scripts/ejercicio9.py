# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm, preprocessing
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV

# Cargar el dataset
data = pd.read_csv('basesDatosPr4IMC/csv/dataset3.csv',header=None)
# Separar entradas de salidas
X = data.iloc[:,:-1].values
y = data.iloc[:,-1].values

# Partición de train y test
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2)
train_index, test_index = list(sss.split(X,y))[0]
train_x = X[train_index]
train_y = y[train_index]
test_x = X[test_index]
test_y = y[test_index]

# Normalización
scaler = preprocessing.StandardScaler()
train_x = scaler.fit_transform(train_x)
test_x = scaler.transform(test_x)
X = scaler.transform(X)

# Entrenar SVM
svm_model = svm.SVC(kernel='rbf', C=0.2, gamma=200)
svm_model.fit(train_x, train_y)


# Calcular CCR
print('CCR Train: {}'.format(svm_model.score(train_x, train_y) * 100))
print('CCR Test: {}'.format(svm_model.score(test_x, test_y) * 100))


# Representar los puntos
plt.figure(1)
plt.clf()
plt.scatter(X[:, 0], X[:, 1], c=y, zorder=10, cmap=plt.cm.Paired)

# Representar el hiperplano separador
plt.axis('tight')
# Extraer límites
x_min = X[:, 0].min()
x_max = X[:, 0].max()
y_min = X[:, 1].min()
y_max = X[:, 1].max()

# Crear un grid con todos los puntos y obtener el valor Z devuelto por la SVM
XX, YY = np.mgrid[x_min:x_max:500j, y_min:y_max:500j]
Z = svm_model.decision_function(np.c_[XX.ravel(), YY.ravel()])

# Hacer un plot a color con los resultados
Z = Z.reshape(XX.shape)
plt.pcolormesh(XX, YY, Z > 0)
plt.contour(XX, YY, Z, colors=['k', 'k', 'k'], linestyles=['--', '-', '--'],
                levels=[-.5, 0, .5])

plt.show()

