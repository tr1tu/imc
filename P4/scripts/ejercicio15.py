# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:14:36 2017

@author: pedroa
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm

# Cargar el dataset
train = pd.read_csv('basesDatosPr4IMC/csv/train_spam.csv',header=None)
test = pd.read_csv('basesDatosPr4IMC/csv/test_spam.csv',header=None)
# Separar entradas de salidas
train_x = train.iloc[:,:-1].values
train_y = train.iloc[:,-1].values
test_x = test.iloc[:,:-1].values
test_y = test.iloc[:,-1].values

# Entrenar el modelo SVM
# Usa función de kernel rbf.
# gamma grande -> radio pequeño
for c in [1e-2, 1e-1, 1e-0, 1e1]:
	print('C={}'.format(c))
	svm_model = svm.SVC(kernel='linear',C=c)
	svm_model.fit(train_x, train_y)
	train_ccr = svm_model.score(train_x, train_y) * 100
	test_ccr = svm_model.score(test_x, test_y) * 100
	
	print('C={}, Train CCR: {}, Test CCR: {}'.format(c, train_ccr, test_ccr))
	
	pred_test = svm_model.predict(test_x)

	errors = np.nonzero(np.equal(pred_test, test_y) == False)	
	errors_pred = np.array(pred_test[errors])
	errors_true = np.array(test_y[errors])
	
	print(np.vstack((errors, errors_pred, errors_true)))