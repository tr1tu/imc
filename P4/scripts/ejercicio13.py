# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn import svm, preprocessing
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV

import ajustecv

# Cargar el dataset
train = pd.read_csv('basesDatosPr4IMC/csv/train_nomnist.csv',header=None)
test = pd.read_csv('basesDatosPr4IMC/csv/test_nomnist.csv',header=None)
# Separar entradas de salidas
train_x = train.iloc[:,:-1].values
train_y = train.iloc[:,-1].values
test_x = test.iloc[:,:-1].values
test_y = test.iloc[:,-1].values


# Normalización
scaler = preprocessing.StandardScaler()
train_x = scaler.fit_transform(train_x)
test_x = scaler.transform(test_x)

# Entrenar SVM
svm_model = svm.SVC(kernel='rbf')

# Ajuste de parámetros
Cs = np.logspace(-10, 15, num=16, base=2)
Gs = np.logspace(-15, 8, num=12, base=2)
#optimo = ajustecv.FitCV(estimator=svm_model, params=dict(C=Cs,gamma=Gs), nfolds=5, verbose=2)
optimo = GridSearchCV(estimator=svm_model, param_grid=dict(C=Cs,gamma=Gs), n_jobs=-1, cv=5, verbose=1)

# Entrenar el mejor modelo
optimo.fit(train_x, train_y)
#svm_model.fit(train_x, train_y)
print('Mejores parámetros: C={}, gamma={}'.format(optimo.best_params_['C'], optimo.best_params_['gamma']))

# Calcular CCR
print('CCR Train: {}'.format(optimo.score(train_x, train_y) * 100))
print('CCR Test: {}'.format(optimo.score(test_x, test_y) * 100))