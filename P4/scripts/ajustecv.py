# -*- coding: utf-8 -*-
"""
Created on Sat Dec  9 13:56:22 2017

@author: victor
"""

import numpy as np

from sklearn.model_selection import StratifiedKFold

class FitCV:
	
	"""
	FitCV find out the best values of the given parameters for the given estimator using the given dataset.
	
	Parameters
	----------
	estimator : any estimator object
		Estimator object that will be used.
		
	params : dictioanry
		Dictionary that contains the name and the values list of the desired parameters.
		
	nfolds : int
		Number of folds that will be made.
		
	verbose : int, default=0
		Verbosity level (0, 1 or 2). Higher verbosity will output and higher ammount of information.	
		
		
	Examples
	--------
	>>> svm_model = svm.SVC(kernel='rbf')
	>>> Cs = np.logspace(-10, 15, num=16, base=2)
	>>> Gs = np.logspace(-15, 8, num=12, base=2)
	>>> best = ajustecv.FitCV(estimator=svm_model, params=dict(C=Cs,gamma=Gs),
						nfolds=5, verbose=0)	
	>>> best.fit(train_x, train_y)
	>>> print('Best parameters: C={}, gamma={}'.format(best.best_params['C'],
						best.best_params['gamma']))	
	>>> # Compute CCR
	>>> print('CCR Train: {}'.format(best.score(train_x, train_y) * 100))
	>>> print('CCR Test: {}'.format(best.score(test_x, test_y) * 100))
	"""	
	
	def __init__(self, estimator, params, nfolds, verbose=0):
		self.estimator = estimator
		self.params = params
		self.nfolds = nfolds
		self.best_params = dict()
		self.best_score = 0
		self.verbose = verbose
		
	def fit(self, X,y):
		# Get params names
		params_names = self.params.keys()
		# Get params values
		params_values = self.params.values()
		# Get number of parameters
		n_params = len(params_names)
		# Construct parameters grid
		grid = np.meshgrid(*params_values)
		#Get number of rows
		n_rows = len(grid[0])
		#Get number of cols
		n_cols = len(grid[0][0])
		
		# KFold
		skf = StratifiedKFold(n_splits=self.nfolds)	
		spl = list(skf.split(X,y))
		
		# Number of total iterations
		total_iterations = n_rows * n_cols
		current_iteration = 1

		for i in range(n_rows):
			for j in range(n_cols):
				d = dict()
				# Fill the dictionary with all the parameters
				for k, param in zip(range(n_params), params_names):
					d[param] = grid[k][i][j]
				
				sum_score = 0
				for fold in spl:
					train_x = X[fold[0]]
					train_y = y[fold[0]]
					val_x = X[fold[1]]
					val_y = y[fold[1]]
				
					# Set params in the estimator
					self.estimator.__init__(**d)
					# Fit
					self.estimator.fit(train_x,train_y)
					# Get ccr
					subscore = self.estimator.score(val_x,val_y)
					sum_score += subscore
					self.__log('Params: {} Fold score: {}'.format(d,subscore), 3)
			
				# Save the best params
				mean_score = sum_score / self.nfolds
				self.__log('({}/{}) Params: {} Mean score: {}'.format(current_iteration, total_iterations, d, mean_score), 2)
				current_iteration += 1
				if mean_score > self.best_score:
					self.best_score = mean_score
					self.best_params = d
					self.__log('Params: {} New best score: {}'.format(self.best_params, self.best_score), 1)
					
					
		# Train the model with the best parameters found
		self.estimator.__init__(**self.best_params)
		self.estimator.fit(train_x, train_y)
					

	def score(self, X, y):
		return self.estimator.score(X,y)
		
	def predict(self, X):
		return self.estimator.predict(X)

	def predict_proba(self, X):
		return self.estimator.predict_proba(X)
		
	def decision_function(self, X):
		return self.estimator.decision_function(X)

	def __log(self, msg, verbositylevel):
		if self.verbose >= verbositylevel:
			print(msg)			