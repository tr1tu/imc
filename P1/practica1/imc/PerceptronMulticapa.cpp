/*********************************************************************
 * File  : PerceptronMulticapa.cpp
 * Date  : 2017
 *********************************************************************/

#include "PerceptronMulticapa.h"
#include "util.h"


#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>


using namespace imc;
using namespace std;
using namespace util;

// ------------------------------
// CONSTRUCTOR: Dar valor por defecto a todos los parámetros
PerceptronMulticapa::PerceptronMulticapa(){
	nNumCapas = 3; // 1 oculta + 1 salida
	pCapas = NULL; //Dejamos la reserva para la funcion inicializar.
	dEta = 0.1;
	dMu = 0.9;
	dValidacion = 0.0;
	dDecremento = 1;
}

// ------------------------------
// Reservar memoria para las estructuras de datos
int PerceptronMulticapa::inicializar(int nl, int npl[]) {
	pCapas = new Capa[nl];

	// Capa de entrada
	pCapas[0].nNumNeuronas = npl[0];
	pCapas[0].pNeuronas = new Neurona[npl[0]];

	//Capas ocultas y capa de salida
	for(int i = 1; i < nl; i++)
	{
		pCapas[i].nNumNeuronas = npl[i];
		pCapas[i].pNeuronas = new Neurona[npl[i]];

		for(int j = 0; j < npl[i]; j++)
		{
			pCapas[i].pNeuronas[j].w = new double[pCapas[i-1].nNumNeuronas + 1]; // + 1 para sesgo
			pCapas[i].pNeuronas[j].deltaW = new double[pCapas[i-1].nNumNeuronas + 1];
			pCapas[i].pNeuronas[j].ultimoDeltaW = new double[pCapas[i-1].nNumNeuronas + 1];
			pCapas[i].pNeuronas[j].wCopia = new double[pCapas[i-1].nNumNeuronas + 1];
		}
	}

	nNumCapas = nl;

	return 1;
}


// ------------------------------
// DESTRUCTOR: liberar memoria
PerceptronMulticapa::~PerceptronMulticapa() {
	liberarMemoria();

}


// ------------------------------
// Liberar memoria para las estructuras de datos
void PerceptronMulticapa::liberarMemoria() {
	for(int i = 1; i < nNumCapas; i++)
	{
		// La capa de entrada no tiene pesos.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			delete[] pCapas[i].pNeuronas[j].w;
			delete[] pCapas[i].pNeuronas[j].deltaW;
			delete[] pCapas[i].pNeuronas[j].ultimoDeltaW;
			delete[] pCapas[i].pNeuronas[j].wCopia;
		}

		delete[] pCapas[i].pNeuronas;
	}

	delete[] pCapas;
}

// ------------------------------
// Rellenar todos los pesos (w) aleatoriamente entre -1 y 1
void PerceptronMulticapa::pesosAleatorios() {
	// La capa de entrada no tiene pesos.
	for(int i = 1; i < nNumCapas; i++)
	{
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++) //<= por el sesgo.
			{
				pCapas[i].pNeuronas[j].w[k] = ((double) rand() / (RAND_MAX)) * 2 - 1;
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
				pCapas[i].pNeuronas[j].deltaW[k] = 0;
				pCapas[i].pNeuronas[j].ultimoDeltaW[k] = 0;
			}
		}
	}
}

// ------------------------------
// Alimentar las neuronas de entrada de la red con un patrón pasado como argumento
void PerceptronMulticapa::alimentarEntradas(double* input) {
	for(int i = 0; i < pCapas[0].nNumNeuronas; i++)
	{
		pCapas[0].pNeuronas[i].x = input[i];
	}
}

// ------------------------------
// Recoger los valores predichos por la red (out de la capa de salida) y almacenarlos en el vector pasado como argumento
void PerceptronMulticapa::recogerSalidas(double* output)
{
	//Reservamos el vector.

	for(int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++)
	{
		output[i] = pCapas[nNumCapas-1].pNeuronas[i].x;
	}
}

// ------------------------------
// Hacer una copia de todos los pesos (copiar w en copiaW)
void PerceptronMulticapa::copiarPesos() {
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
}

// ------------------------------
// Restaurar una copia de todos los pesos (copiar copiaW en w)
void PerceptronMulticapa::restaurarPesos() {
	for(int i = 1; i < nNumCapas; i++)
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
				for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
					pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].wCopia[k];
}

// ------------------------------
// Calcular y propagar las salidas de las neuronas, desde la primera capa hasta la última
void PerceptronMulticapa::propagarEntradas() {
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			double net = 0;
	
			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				net += pCapas[i-1].pNeuronas[k].x * pCapas[i].pNeuronas[j].w[k];
			}

			net += pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas]; // Sesgo

			pCapas[i].pNeuronas[j].x = (1.0/(1+exp(-net)));
		}
}

// ------------------------------
// Calcular el error de salida (MSE) del out de la capa de salida con respecto a un vector objetivo y devolverlo
double PerceptronMulticapa::calcularErrorSalida(double* target) {
	double mse = 0;

	for(int i = 0; i < pCapas[nNumCapas - 1].nNumNeuronas; i++)
	{
		mse += pow(target[i] - pCapas[nNumCapas - 1].pNeuronas[i].x, 2);
	}

	mse = mse / pCapas[nNumCapas - 1].nNumNeuronas;

	return mse;
}


// ------------------------------
// Retropropagar el error de salida con respecto a un vector pasado como argumento, desde la última capa hasta la primera
void PerceptronMulticapa::retropropagarError(double* objetivo) {
	// Neuronas de la capa de salida
	for (int i = 0; i < pCapas[nNumCapas - 1].nNumNeuronas; i++)
	{
		double out = pCapas[nNumCapas - 1].pNeuronas[i].x;
		pCapas[nNumCapas - 1].pNeuronas[i].dX = - (objetivo[i] - out) * out * (1 - out);
	}

	// Neuronas de las capas ocultas
	for (int i = nNumCapas - 2; i >= 1; i--) // i >= 1, en la capa de entrada no hay delta.
	{
		for (int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			double s = 0.0;
			double out = pCapas[i].pNeuronas[j].x;
			for(int k = 0; k < pCapas[i+1].nNumNeuronas; k++)
			{
				s += pCapas[i+1].pNeuronas[k].w[j] * pCapas[i+1].pNeuronas[k].dX;
			}
			pCapas[i].pNeuronas[j].dX = s * out * (1 - out);

		}
	}

	
}

// ------------------------------
// Acumular los cambios producidos por un patrón en deltaW
void PerceptronMulticapa::acumularCambio() {

	// Para cada capa (menos la de entrada)
	for(int i = 1; i < nNumCapas; i++)
	{
		// Para cada neurona de la capa i.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			// Para cada neurona de la capa i - 1
			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				// Calcular el nuevo
				pCapas[i].pNeuronas[j].deltaW[k] += pCapas[i].pNeuronas[j].dX * pCapas[i-1].pNeuronas[k].x;
			}

			// Actualizar el deltaW del sesgo
			pCapas[i].pNeuronas[j].deltaW[pCapas[i-1].nNumNeuronas] += pCapas[i].pNeuronas[j].dX * 1;
		}
	}
}

// ------------------------------
// Actualizar los pesos de la red, desde la primera capa hasta la última
void PerceptronMulticapa::ajustarPesos() {

	for(int i = 1; i < nNumCapas; i++)
	{
		//Calculamos la eta para esta capa usando el factor de decremento.
		double etaI = pow(dDecremento, -(nNumCapas-i)) * dEta;

		// Para cada neurona de la capa i.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			// Para cada neurona de la capa i - 1
			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				// Actualizar pesos
				pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].w[k] - etaI * pCapas[i].pNeuronas[j].deltaW[k]
											  - dMu * etaI * pCapas[i].pNeuronas[j].ultimoDeltaW[k];
			}
			// Actualizar sesgo
			pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] = pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] -
					etaI * pCapas[i].pNeuronas[j].deltaW[pCapas[i-1].nNumNeuronas] - etaI * dEta * pCapas[i].pNeuronas[j].ultimoDeltaW[pCapas[i-1].nNumNeuronas];
		}
	}
}

// ------------------------------
// Imprimir la red, es decir, todas las matrices de pesos
void PerceptronMulticapa::imprimirRed() {
	// Para cada capa menos la de entrada
	for(int i = 1; i < nNumCapas; i++)
	{
		cout << "Capa " << i << endl;
		cout << "------------------" << endl;

		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			cout << pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] << " ";

			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
				cout << pCapas[i].pNeuronas[j].w[k] << " ";

			cout << endl;
		}
	}
}

// ------------------------------
// Simular la red: propagar las entradas hacia delante, retropropagar el error y ajustar los pesos
// entrada es el vector de entradas del patrón y objetivo es el vector de salidas deseadas del patrón
void PerceptronMulticapa::simularRedOnline(double* entrada, double* objetivo) {
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
			{
				// Guardar el ultimo deltaW
				pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
				//Inicializar deltaW
				pCapas[i].pNeuronas[j].deltaW[k] = 0;
			}

		}

	alimentarEntradas(entrada);
	propagarEntradas();
	retropropagarError(objetivo);
	acumularCambio();
	ajustarPesos();
}

// ------------------------------
// Leer una matriz de datos a partir de un nombre de fichero y devolverla
Datos* PerceptronMulticapa::leerDatos(const char *archivo) {

	// Objeto de lectura de fichero
	ifstream f(archivo);

	if(!f.is_open())
		return NULL;

	// Estructura que almacenará los datos
	Datos *datos = new Datos;

	// Lectura de las dimensiones
	f >> datos->nNumEntradas >> datos->nNumSalidas >> datos->nNumPatrones;

	// Reserva de memoria para las entradas y salidas.
	datos->entradas = new double*[datos->nNumPatrones];
	for(int i = 0; i < datos->nNumPatrones; i++)
		datos->entradas[i] = new double[datos->nNumEntradas];
	datos->salidas = new double*[datos->nNumPatrones];
		for(int i = 0; i < datos->nNumPatrones; i++)
			datos->salidas[i] = new double[datos->nNumSalidas];

	// Lectura de los datos
	for (int i = 0; i < datos->nNumPatrones; i++)
	{
		// Leer las entradas
		for (int j = 0; j < datos->nNumEntradas; j++)
			f >> datos->entradas[i][j];

		// Leer las salidas
		for (int j = 0; j < datos->nNumSalidas; j++)
			f >> datos->salidas[i][j];
	}

	// Cerrar el fichero
	f.close();

	// La estructura datos se debe liberar cuando no se vaya a utilizar mas.
	return datos;
}

// ------------------------------
// Entrenar la red on-line para un determinado fichero de datos
void PerceptronMulticapa::entrenarOnline(Datos* pDatosTrain) {
	int i;
	for(i=0; i<pDatosTrain->nNumPatrones; i++){
		simularRedOnline(pDatosTrain->entradas[i], pDatosTrain->salidas[i]);
	}
}

// ------------------------------
// Probar la red con un conjunto de datos y devolver el error MSE cometido
double PerceptronMulticapa::test(Datos* pDatosTest, double * errores) {
	double errorMedio = 0;

	for (int i = 0; i < pDatosTest->nNumPatrones; i++)
	{
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		double error = calcularErrorSalida(pDatosTest->salidas[i]);
		errorMedio += error;

		if(errores != 0)
		{
			errores[i] = error;
		}
	}

	errorMedio /= pDatosTest->nNumPatrones;

	return errorMedio;
}

// ------------------------------
// Ejecutar el algoritmo de entrenamiento durante un número de iteraciones, utilizando pDatosTrain
// Una vez terminado, probar como funciona la red en pDatosTest
// Tanto el error MSE de entrenamiento como el error MSE de test debe calcularse y almacenarse en errorTrain y errorTest
void PerceptronMulticapa::ejecutarAlgoritmoOnline(Datos * pDatosTrain, Datos * pDatosTest, int maxiter, double *errorTrain, double *errorTest)
{
	int countTrain = 0;

	// Inicialización de pesos
	pesosAleatorios();

	double minTrainError = 0;
	double minValidationError = 0;
	int numSinMejorar = 0;
	int numSinMejoraVal = 0;
	double testError = 0;

	Datos * pNuevosDatosTrain = new Datos;
	Datos * pDatosValidacion = new Datos;


	// Generar datos de validación
	if(dValidacion > 0 && dValidacion < 1){

		// Divide el conjunto de train en train y validacion.
		dividirTrain(pDatosTrain, pNuevosDatosTrain, pDatosValidacion, dValidacion);
		// Modificación que solo afecta dentro de esta función
		pDatosTrain = pNuevosDatosTrain;

	}


	// Aprendizaje del algoritmo
	do {
		entrenarOnline(pDatosTrain);
		double trainError = test(pDatosTrain);

		if(countTrain==0 || fabs(trainError - minTrainError) > 0.00001){
			minTrainError = trainError;
			copiarPesos();
			numSinMejorar = 0;
		}
		else{
			numSinMejorar++;
		}

		if(numSinMejorar==50){
			restaurarPesos();
			countTrain = maxiter;
		}

		double validationError = 0;

		// Comprobar condiciones de parada de validación y forzar
		if(dValidacion > 0 && dValidacion < 1)
		{
			validationError = test(pDatosValidacion);
			if (countTrain == 0 || fabs(validationError - minValidationError) > 0.00001)
			{
				minValidationError = validationError;
				copiarPesos();
				numSinMejoraVal = 0;
			}
			else
				numSinMejoraVal++;

			if(numSinMejoraVal == 50)
			{
				restaurarPesos();
				countTrain = maxiter;
			}
		}

		countTrain++;

		double testError = test(pDatosTest);

		cout << "Iteración " << countTrain << "\t Error de entrenamiento: " << trainError << "\t Error de validación: " << validationError << "\t Error de test: " << testError << endl;

	} while ( countTrain<maxiter );

	cout << "PESOS DE LA RED" << endl;
	cout << "===============" << endl;
	imprimirRed();

	cout << "Salida Esperada Vs Salida Obtenida (test)" << endl;
	cout << "=========================================" << endl;
	for(int i=0; i<pDatosTest->nNumPatrones; i++){
		double* prediccion = new double[pDatosTest->nNumSalidas];

		// Cargamos las entradas y propagamos el valor
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		recogerSalidas(prediccion);
		for(int j=0; j<pDatosTest->nNumSalidas; j++)
			cout << "" << pDatosTest->salidas[i][j] << " " << prediccion[j] << " ";
		cout << endl;
		delete[] prediccion;

	}

	testError = test(pDatosTest);
	*errorTest=testError;
	*errorTrain=minTrainError;

	// Liberar memoria
	for(int i = 0; i < pNuevosDatosTrain->nNumPatrones; i++)
	{
		delete[] pNuevosDatosTrain->entradas[i];
		delete[] pNuevosDatosTrain->salidas[i];
	}

	delete[] pNuevosDatosTrain->entradas;
	delete[] pNuevosDatosTrain->salidas;
	delete pNuevosDatosTrain;

	for(int i = 0; i < pDatosValidacion->nNumPatrones; i++)
	{
		delete[] pDatosValidacion->entradas[i];
		delete[] pDatosValidacion->salidas[i];
	}

	delete[] pDatosValidacion->entradas;
	delete[] pDatosValidacion->salidas;
	delete pDatosValidacion;


}

void PerceptronMulticapa::dividirTrain(Datos * pDatosTrain, Datos * pNuevosDatosTrain, Datos * pDatosValidacion, double v)
{
	// Dimensiones de validacion
	pDatosValidacion->nNumPatrones = round(pDatosTrain->nNumPatrones * dValidacion);
	pDatosValidacion->nNumEntradas = pDatosTrain->nNumEntradas;
	pDatosValidacion->nNumSalidas = pDatosTrain->nNumSalidas;

	//Dimensiones del nuevo train
	pNuevosDatosTrain->nNumPatrones = pDatosTrain->nNumPatrones - pDatosValidacion->nNumPatrones;
	pNuevosDatosTrain->nNumEntradas = pDatosTrain->nNumEntradas;
	pNuevosDatosTrain->nNumSalidas = pDatosTrain->nNumSalidas;

	// Vector que contiene los indices de los patrones en un orden aleatorio.
	int * indicesTrain = vectorAleatoriosEnterosSinRepeticion(0, pDatosTrain->nNumPatrones-1, pDatosTrain->nNumPatrones);
	int * indicesValidacion = indicesTrain + pNuevosDatosTrain->nNumPatrones;

	// Nuevos vectores de train (reserva)
	pNuevosDatosTrain->entradas = new double*[pNuevosDatosTrain->nNumPatrones];
	pNuevosDatosTrain->salidas = new double*[pNuevosDatosTrain->nNumPatrones];
	for(int i = 0; i < pNuevosDatosTrain->nNumPatrones; i++)
	{
		pNuevosDatosTrain->entradas[i] = new double[pNuevosDatosTrain->nNumEntradas];
		pNuevosDatosTrain->salidas[i] = new double[pNuevosDatosTrain->nNumSalidas];
	}

	// Vectores de validacion (reserva)
	pDatosValidacion->entradas = new double*[pDatosValidacion->nNumPatrones];
	pDatosValidacion->salidas = new double*[pDatosValidacion->nNumPatrones];
	for(int i = 0; i < pDatosValidacion->nNumPatrones; i++)
	{
		pDatosValidacion->entradas[i] = new double[pDatosValidacion->nNumEntradas];
		pDatosValidacion->salidas[i] = new double[pDatosValidacion->nNumSalidas];
	}


	// Rellenar ambos vectores
	// Los indices de los patrones de train estan en indicesTrain (pNuevosDatosTrain->nNumPatrones)
	for(int i = 0; i < pNuevosDatosTrain->nNumPatrones; i++) // Train
	{
		int idx = indicesTrain[i];

		for(int j = 0; j < pNuevosDatosTrain->nNumEntradas; j++)
			pNuevosDatosTrain->entradas[i][j] = pDatosTrain->entradas[idx][j];

		for(int j = 0; j < pNuevosDatosTrain->nNumSalidas; j++)
			pNuevosDatosTrain->salidas[i][j] = pDatosTrain->salidas[idx][j];
	}

	for(int i = 0; i < pDatosValidacion->nNumPatrones; i++) // Train
	{
		int idx = indicesValidacion[i];

		for(int j = 0; j < pDatosValidacion->nNumEntradas; j++)
			pDatosValidacion->entradas[i][j] = pDatosTrain->entradas[idx][j];

		for(int j = 0; j < pDatosValidacion->nNumSalidas; j++)
			pDatosValidacion->salidas[i][j] = pDatosTrain->salidas[idx][j];
	}

	delete[] indicesTrain;

}


bool PerceptronMulticapa::guardarPesos(const char * archivo)
{
	// Objeto de escritura de fichero
	ofstream f(archivo);

	if(!f.is_open())
		return false;

	// Escribir el numero de capas y el numero de neuronas en cada capa en la primera linea.
	f << nNumCapas;

	for(int i = 0; i < nNumCapas; i++)
		f << " " << pCapas[i].nNumNeuronas;
	f << endl;

	// Escribir los pesos de cada capa
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			for(int k = 0; k < pCapas[i-1].nNumNeuronas + 1; k++)
				f << pCapas[i].pNeuronas[j].w[k] << " ";

	f.close();

	return true;

}

bool PerceptronMulticapa::cargarPesos(const char * archivo)
{
	// Objeto de lectura de fichero
	ifstream f(archivo);

	if(!f.is_open())
		return false;

	// Número de capas y de neuronas por capa.
	int nl;
	int *npl;

	// Leer número de capas.
	f >> nl;

	npl = new int[nl];

	// Leer número de neuronas en cada capa.
	for(int i = 0; i < nl; i++)
		f >> npl[i];

	// Inicializar vectores y demás valores.
	inicializar(nl, npl);

	// Leer pesos.
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			for(int k = 0; k < pCapas[i-1].nNumNeuronas + 1; k++)
				f >> pCapas[i].pNeuronas[j].w[k];

	f.close();
	delete[] npl;

	return true;
}
