//============================================================================
// Introducción a los Modelos Computacionales
// Name        : practica1.cpp
// Author      : Pedro A. Gutiérrez
// Version     :
// Copyright   : Universidad de Córdoba
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include <math.h>
#include "imc/PerceptronMulticapa.h"


using namespace imc;
using namespace std;

struct CLIParams
{
  CLIParams ()
    : train(""),
	  test(""),
	  weights(""),
	  it(1000),
	  nOcultas(1),
	  nNeuronas(5),
	  eta(0.1),
	  mu(0.9),
	  rVal(0.0),
	  fDec(1)
    {}
  ~CLIParams(){}
  char train[250];
  char test[250];
  char weights[250];
  int it; // Iteraciones del bucle externo
  int nOcultas;
  int nNeuronas;
  double eta;
  double mu;
  double rVal; // Ratio de patrones para validacion
  double fDec; // Factor de decremento


};

static void mostrarUso (const char * progname);
static int parseCLI (int argc, char* const* argv, CLIParams& params);

int main(int argc, char **argv) {
	// Procesar los argumentos de la línea de comandos
	CLIParams params;
	int numArgs;

	numArgs = parseCLI(argc, argv, params);

	if(numArgs < 2)
	{
		mostrarUso(argv[0]);
		return -1;
	}

	if(params.nOcultas < 1)
	{
		cerr << "Se debe usar al menos 1 capa oculta." << endl;
		exit(-1);
	}

	if(params.nNeuronas < 1)
	{
		cerr << "Se debe usar al menos 1 neurona por capa." << endl;
		exit(-1);
	}

	// Objeto perceptrón multicapa
	PerceptronMulticapa mlp;

	// Parámetros del mlp. Por ejemplo, mlp.dEta = valorQueSea;
	mlp.dEta = params.eta;
	mlp.dMu = params.mu;
	mlp.dValidacion = params.rVal;
	mlp.dDecremento = params.fDec;
	int nCapasOcultas = params.nOcultas;
	int nNeuronas = params.nNeuronas;
	int iteraciones = params.it;

	// Lectura de datos de entrenamiento y test: llamar a mlp.leerDatos(...)
	Datos *pDatosTrain, *pDatosTest;
	pDatosTrain = mlp.leerDatos(params.train);
	if(pDatosTrain == NULL)
	{
		cerr << "El conjunto de datos de train no es válido. No se puede continuar." << endl;
		exit(-1);
	}
	pDatosTest = mlp.leerDatos(params.test);
	if(pDatosTest == NULL)
	{
		cerr << "No se especificó un conjunto de test válido. Usando conjunto de train para test." << endl;
		pDatosTest = mlp.leerDatos(params.train); // Si no hay test, usamos los de train.
	}

	// Inicializar vector topología
	int *topologia = new int[nCapasOcultas+2];
	topologia[0] = pDatosTrain->nNumEntradas;
	for(int i=1; i<(nCapasOcultas+2-1); i++)
		topologia[i] = nNeuronas;
	topologia[nCapasOcultas+2-1] = pDatosTrain->nNumSalidas;

	// Inicializar red con vector de topología
	mlp.inicializar(nCapasOcultas+2,topologia);


    // Semilla de los números aleatorios
    int semillas[] = {10,20,30,40,50};
    double *erroresTest = new double[5];
    double *erroresTrain = new double[5];
    double mejorErrorTest = 1;
    for(int i=0; i<5; i++){
    	cout << "**********" << endl;
    	cout << "SEMILLA " << semillas[i] << endl;
    	cout << "**********" << endl;
		srand(semillas[i]);
		mlp.ejecutarAlgoritmoOnline(pDatosTrain,pDatosTest,iteraciones,&(erroresTrain[i]),&(erroresTest[i]));
		cout << "Finalizamos => Error de test final: " << erroresTest[i] << endl;

		// Guardamos los pesos cada vez que encontremos un modelo mejor.
		if(strcmp(params.weights, "") != 0 && erroresTest[i] <= mejorErrorTest)
		{
			mlp.guardarPesos(params.weights);
			mejorErrorTest = erroresTest[i];
		}
	}

    cout << "HEMOS TERMINADO TODAS LAS SEMILLAS" << endl;

    double mediaErrorTest = 0, desviacionTipicaErrorTest = 0;
    double mediaErrorTrain = 0, desviacionTipicaErrorTrain = 0;
    
    for (int i = 0; i < 5; i++)
    {
    	mediaErrorTest += erroresTest[i];
    	mediaErrorTrain += erroresTrain[i];
    }

    mediaErrorTest /= 5;
    mediaErrorTrain /= 5;

    for (int i = 0; i < 5; i++)
    {
    	desviacionTipicaErrorTest += pow(erroresTest[i] - mediaErrorTest, 2);
    	desviacionTipicaErrorTrain += pow(erroresTrain[i] - mediaErrorTrain, 2);
    }

    desviacionTipicaErrorTest = sqrt(desviacionTipicaErrorTest/5);
    desviacionTipicaErrorTrain = sqrt(desviacionTipicaErrorTrain/5);

    // Calcular medias y desviaciones típicas de entrenamiento y test

    cout << "INFORME FINAL" << endl;
    cout << "*************" << endl;
    cout << "Error de entrenamiento (Media +- DT): " << mediaErrorTrain << " +- " << desviacionTipicaErrorTrain << endl;
    cout << "Error de test (Media +- DT):          " << mediaErrorTest << " +- " << desviacionTipicaErrorTest << endl;


    return EXIT_SUCCESS;
}

static void mostrarUso (const char * progname)
{
  cout << "Modelo de perceptrón multicapa" << std::endl;
  cout << "Uso: " << progname << " -t <train_dataset> [-T <test_dataset> | -w <weights_output> | -i <iterations> | -l <hidden layers> | -e <eta value> | -m <mu value> | -d <decrement factor> | -v <% validation>]" << std::endl;
}

static int parseCLI (int argc, char* const* argv, CLIParams& params)
{
  int opcion;
  while ((opcion = getopt (argc, argv, "t:T:w:i:l:h:e:m:v:d:")) != -1)
  {
    switch (opcion)
    {

      case 't':
    	  strcpy(params.train, optarg);
	break;

      case 'T':
    	  strcpy(params.test, optarg);
	break;

      case 'w':
    	  strcpy(params.weights, optarg);
    break;

      case 'i':
    	  params.it = atoi(optarg);
	break;

      case 'l':
    	  params.nOcultas = atoi(optarg);
	break;

      case 'h':
		  params.nNeuronas = atoi(optarg);
	break;

      case 'e':
		  params.eta = atof(optarg);
	break;

      case 'm':
		  params.mu = atof(optarg);
	break;

      case 'v':
		  params.rVal = atof(optarg);
	break;

      case 'd':
		  params.fDec = atof(optarg);
	break;

      case '?': // en caso de error getopt devuelve el caracter ?

	if (isprint (optopt))
	  cerr << "Error: Opción desconocida \'" << optopt
	    << "\'" << std::endl;
	else
	  cerr << "Error: Caracter de opcion desconocido \'x" << hex << optopt
	    << "\'" << std::endl;
	mostrarUso(argv[0]);
	exit (EXIT_FAILURE);

	// en cualquier otro caso lo consideramos error grave y salimos
      default:
	cerr << "Error: línea de comandos errónea." << endl;
	mostrarUso(argv[0]);
	exit(EXIT_FAILURE);
    }  // case

  }// while
  return optind;
}


