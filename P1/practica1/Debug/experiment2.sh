#!/bin/bash

if [ ! -d results2 ];
then
	mkdir results2
fi


# Experimentos de xor
# 2 capas ocultas de 100 neuronas
for v in 0; do
	for f in 1 2; do
		startTime=$(date +%s)
		./practica1 -t  train_xor.dat -T test_xor.dat -i 1000 -l 2 -h 100 -e 0.1 -m 0.9 -v $v -d $f > "results2/xor_"$v"_"$f".txt"
		echo "XOR con v=$v y f=$f Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de sin
# 2 capas ocultas de 100 neuronas
for v in 0 0.1 0.2; do
	for f in 1 2; do
		startTime=$(date +%s)
		./practica1 -t  train_sin.dat -T test_sin.dat -i 1000 -l 2 -h 100 -e 0.1 -m 0.9 -v $v -d $f > "results2/sin_"$v"_"$f".txt"
		echo "Sin con v=$v y f=$f Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de forest
# 1 capas oculta de 10 neuronas
for v in 0 0.1 0.2; do
	for f in 1 2; do
		startTime=$(date +%s)
		./practica1 -t  train_forest.dat -T test_forest.dat -i 1000 -l 2 -h 2 -e 0.1 -m 0.9 -v $v -d $f > "results2/forest_"$v"_"$f".txt"
		echo "Forest con v=$v y f=$f Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de parkinsons
# 2 capas ocultas de 100 neuronas
for v in 0 0.1 0.2; do
	for f in 1 2; do
		startTime=$(date +%s)
		./practica1 -t  train_parkinsons.dat -T test_parkinsons.dat -i 1000 -l 2 -h 100 -e 0.1 -m 0.9 -v $v -d $f > "results2/parkinsons_"$v"_"$f".txt"
		echo "Parkinsons con v=$v y f=$f Tiempo: "$(( $(date +%s) - startTime ))
	done
done