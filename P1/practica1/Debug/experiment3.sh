#!/bin/bash

if [ ! -d results3 ];
then
	mkdir results3
fi


# Experimentos de xor
# XOR: 2 capas ocultas de 100 neuronas. v = 0 y F = 1
for e in 0.1 0.2 0.3 0.4 0.5; do
	for m in 0.9 0.8 0.7 0.6 0.5; do
		startTime=$(date +%s)
		./practica1 -t train_xor.dat -T test_xor.dat -i 1000 -l 2 -h 100 -e $e -m $m -v 0 -d 1 > "results3/xor_"$e"_"$m".txt"
		echo "XOR con e=$e y m=$m Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de sin
# Sin: 2 capas ocultas de 100 neuronas. v = 0.1 y F = 1
for e in 0.1 0.2 0.3 0.4 0.5; do
	for m in 0.9 0.8 0.7 0.6 0.5; do
		startTime=$(date +%s)
		./practica1 -t train_sin.dat -T test_sin.dat -i 1000 -l 2 -h 100 -e $e -m $m -v 0.1 -d 1 > "results3/sin_"$e"_"$m".txt"
		echo "Sin con e=$e y m=$m Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de forest
# Forest: 2 capas ocultas de 2 neuronas. v = 0.2 y F = 2
for e in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1; do
	for m in 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1; do
		startTime=$(date +%s)
		./practica1 -t train_forest.dat -T test_forest.dat -i 1000 -l 2 -h 2 -e $e -m $m -v 0.2 -d 2 > "results3/forest_"$e"_"$m".txt"
		echo "Forest con e=$e y m=$m Tiempo: "$(( $(date +%s) - startTime ))
	done
done

# Experimentos de parkinsons
# Parkinsons: 2 capas ocultas de 100 neuronas. v = 0 y F = 1
for e in 0.1 0.2 0.3 0.4 0.5; do
	for m in 0.9 0.8 0.7 0.6 0.5; do
		startTime=$(date +%s)
		./practica1 -t train_parkinsons.dat -T test_parkinsons.dat -i 1000 -l 2 -h 100 -e $e -m $m -v 0 -d 1 > "results3/parkinsons_"$e"_"$m".txt"
		echo "Parkinsons con e=$e y m=$m Tiempo: "$(( $(date +%s) - startTime ))
	done
done