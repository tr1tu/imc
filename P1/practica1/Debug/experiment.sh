#!/bin/bash

if [ ! -d results ];
then
	mkdir results
fi

for bd in xor sin forest parkinsons; do
	for l in 1 2; do
		for h in 2 5 10 25 50 100; do
			v=0.1

			if [ $bd == "xor" ]; then
				v=0.0
			fi

			echo "$bd con $l capas con $h neuronas"
			./practica1 -t train_$bd.dat -T test_$bd.dat -i 1000 -l $l -h $h -e 0.1 -m 0.9 -v $v -d 1 > "results/"$bd"_"$l"_"$h".txt"
		done 
	done
done