#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include <math.h>
#include "imc/PerceptronMulticapa.h"


using namespace imc;
using namespace std;

struct CLIParams
{
  CLIParams ()
    :
      weights(""),
	  test("")
    {}
  ~CLIParams(){}
  char weights[250];
  char test[250];


};

static void mostrarUso (const char * progname);
static int parseCLI (int argc, char* const* argv, CLIParams& params);

int main(int argc, char **argv) {
	// Procesar los argumentos de la línea de comandos
	CLIParams params;
	int numArgs;

	numArgs = parseCLI(argc, argv, params);

	if(numArgs < 3)
	{
		mostrarUso(argv[0]);
		return -1;
	}

	// Objeto perceptrón multicapa
	PerceptronMulticapa mlp;

	// Inicializar red con vector de topología
	if(!mlp.cargarPesos(params.weights))
	{
		cerr << "Error al cargar los pesos. No se puede continuar." << endl;
		exit(-1);
	}

	// Lectura de datos de entrenamiento y test: llamar a mlp.leerDatos(...)
	Datos *pDatosTest;
	pDatosTest = mlp.leerDatos(params.test);
	if(pDatosTest == NULL)
	{
		cerr << "El conjunto de datos de test no es válido. No se puede continuar." << endl;
		exit(-1);
	}

	double * errores = new double[pDatosTest->nNumPatrones];

	mlp.test(pDatosTest, errores);

	for(int i = 0; i < pDatosTest->nNumPatrones; i++)
		cout << errores[i] << endl;

	delete[] errores;


    return EXIT_SUCCESS;
}

static void mostrarUso (const char * progname)
{
  cout << "Predicción de modelo de perceptrón multicapa" << std::endl;
  cout << "Uso: " << progname << " -w <weights_file> -T <test_dataset>" << std::endl;
}

static int parseCLI (int argc, char* const* argv, CLIParams& params)
{
  int opcion;
  while ((opcion = getopt (argc, argv, "w:T:")) != -1)
  {
    switch (opcion)
    {

      case 'w':
    	  strcpy(params.weights, optarg);
	break;

      case 'T':
    	  strcpy(params.test, optarg);
	break;

      case '?': // en caso de error getopt devuelve el caracter ?

	if (isprint (optopt))
	  cerr << "Error: Opción desconocida \'" << optopt
	    << "\'" << std::endl;
	else
	  cerr << "Error: Caracter de opcion desconocido \'x" << hex << optopt
	    << "\'" << std::endl;
	mostrarUso(argv[0]);
	exit (EXIT_FAILURE);

	// en cualquier otro caso lo consideramos error grave y salimos
      default:
	cerr << "Error: línea de comandos errónea." << endl;
	mostrarUso(argv[0]);
	exit(EXIT_FAILURE);
    }  // case

  }// while
  return optind;
}


