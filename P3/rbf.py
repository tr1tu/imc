#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 12:37:04 2016

@author: pagutierrez
"""

# Incluir todos los import necesarios
import click
import numpy as np
import pandas as pd
import time

from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.cluster import KMeans
from scipy.spatial.distance import pdist, squareform
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import confusion_matrix

@click.group()
def cli():
    pass

@cli.command(name='entrenar', help='Entrenar con los datos y los parametros indicados')
@click.option('--train_file', '-t', default=None, required=True,
              help=u'Fichero con los datos de entrenamiento.')
              
@click.option('--test_file', '-T', default=None, required=False,
              help=u'Fichero con los datos de test. Si no se especifica, se utilizará el de entrenamiento')
              
@click.option('--classification', '-c', is_flag=True,
              help=u'Realizar clasificación en lugar de regresión')
              
@click.option('--ratio_rbf', '-r', default=0.1, required=False,
              help=u'Relación de neuronas RBF con respecto al total de patrones de entrenamiento')
              
@click.option('--l2', '-l', is_flag=True,
              help=u'Utilizar la norma L2 en lugar de la norma L1')
              
@click.option('--eta', '-e', default=0.01, required=False,
              help=u'Tasa de aprendizaje')
@click.option('--outputs', '-o', default=1, required=False,
              help=u'Número de salidas')
@click.option('--showerrors', '-x', is_flag=True,
              help=u'Mostrar la matriz de confusión y las predicciones erróneas')
              
def entrenar(train_file, test_file, classification, ratio_rbf, l2, eta, outputs, showerrors):
    if test_file == None:
        test_file = train_file
    entrenar_rbf_total(train_file, test_file, classification, ratio_rbf, l2, eta, outputs, showerrors)
              
def entrenar_rbf_total(train_file, test_file, classification, ratio_rbf, l2, eta, outputs, showerrors = False):
    """ Modelo de aprendizaje supervisado mediante red neuronal de tipo RBF.
        Ejecución de 5 semillas.
    """
    train_mses = np.empty(5)
    train_ccrs = np.empty(5)
    test_mses = np.empty(5)
    test_ccrs = np.empty(5)
    num_coefs = np.empty(5)
    elapsed_times = np.empty(5)

    for s in range(10,60,10):   
        print("-----------")
        print("Semilla: %d" % s)
        print("-----------")     
        np.random.seed(s)
        elapsed_times[s//10-1] = time.time()
        train_mses[s//10-1], test_mses[s//10-1], train_ccrs[s//10-1], test_ccrs[s//10-1], num_coefs[s//10-1], \
            cmatrix, errors = entrenar_rbf(train_file, test_file, classification, ratio_rbf, l2, eta, outputs)
        elapsed_times[s//10-1] = time.time() - elapsed_times[s//10-1]
        print("MSE de entrenamiento: %f" % train_mses[s//10-1])
        print("MSE de test: %f" % test_mses[s//10-1])
        print("CCR de entrenamiento: %.2f%%" % train_ccrs[s//10-1])
        print("CCR de test: %.2f%%" % test_ccrs[s//10-1])
        print("Número de coeficientes: %d" % num_coefs[s//10-1])
        print("Tiempo transcurrido: %.2f" % elapsed_times[s//10-1])
        if classification and showerrors:
            print(cmatrix)
            print('Patrones de test con error:\n{}'.format(errors))

    
    train_mse_mean = np.mean(train_mses)
    train_mse_std = np.std(train_mses)
    test_mse_mean = np.mean(test_mses)
    test_mse_std = np.std(test_mses)
    train_ccr_mean = np.mean(train_ccrs)
    train_ccr_std = np.std(train_ccrs)
    test_ccr_mean = np.mean(test_ccrs)
    test_ccr_std = np.std(test_ccrs)
    num_coefs_mean = np.mean(num_coefs)
    elapsed_times_mean = np.mean(elapsed_times)
    
    print("*********************")
    print("Resumen de resultados")
    print("*********************")
    print("MSE de entrenamiento: %f +- %f" % (train_mse_mean, train_mse_std))
    print("MSE de test: %f +- %f" % (test_mse_mean, test_mse_std))
    print("CCR de entrenamiento: %.2f%% +- %.2f%%" % (train_ccr_mean, train_ccr_std))
    print("CCR de test: %.2f%% +- %.2f%%" % (test_ccr_mean, test_ccr_std))
    print("Número de coeficientes: %.2f" % num_coefs_mean)
    print("Tiempo medio: %.2f" % elapsed_times_mean)
        
    return train_mse_mean, test_mse_mean, train_ccr_mean, test_ccr_mean, num_coefs_mean

def entrenar_rbf(train_file, test_file, classification, ratio_rbf, l2, eta, outputs):
    """ Modelo de aprendizaje supervisado mediante red neuronal de tipo RBF.
        Una única ejecución.
        Recibe los siguientes parámetros:
            - train_file: nombre del fichero de entrenamiento.
            - test_file: nombre del fichero de test.
            - classification: True si el problema es de clasificacion.
            - ratio_rbf: Ratio (en tanto por uno) de neuronas RBF con 
              respecto al total de patrones.
            - l2: True si queremos utilizar L2 para la Regresión Logística. 
              False si queremos usar L1 (para regresión logística).
            - eta: valor del parámetro de regularización para la Regresión 
              Logística.
            - outputs: número de salidas.
        Devuelve:
            - train_mse: Error de tipo Mean Squared Error en entrenamiento. 
              En el caso de clasificación, calcularemos el MSE de las 
              probabilidades predichas frente a las objetivo.
            - test_mse: Error de tipo Mean Squared Error en test. 
              En el caso de clasificación, calcularemos el MSE de las 
              probabilidades predichas frente a las objetivo.
            - train_ccr: Error de clasificación en entrenamiento. 
              En el caso de regresión, devolvemos un cero.
            - test_ccr: Error de clasificación en test. 
              En el caso de regresión, devolvemos un cero.
    """
    train_inputs, train_outputs, test_inputs, test_outputs = lectura_datos(train_file, 
                                                                           test_file,
                                                                           outputs)

    # Obtener num_rbf a partir de ratio_rbf
    num_rbf = int(round(len(train_inputs) * ratio_rbf))
    print("Número de RBFs utilizadas: %d" %(num_rbf))
    kmedias, distancias, centros = clustering(classification, train_inputs, 
                                              train_outputs, num_rbf)
    
    radios = calcular_radios(centros, num_rbf)
    
    matriz_r = calcular_matriz_r(distancias, radios)

    if not classification:
        coeficientes = invertir_matriz_regresion(matriz_r, train_outputs)
    else:
        # .ravel() para pasar la columna a vector
        logreg = logreg_clasificacion(matriz_r, train_outputs.ravel(), eta, l2)
        
    
    """
          Calcular las distancias de los centroides a los patrones de test
          y la matriz R de test
    """

    # Matriz de distancias de cada patron a cada rbf
    distancias_test = kmedias.transform(test_inputs)   

    matriz_r_test = calcular_matriz_r(distancias_test, radios)                                                             

    train_ccr = 0
    test_ccr = 0

    if not classification:
        """
              Obtener las predicciones de entrenamiento y de test y calcular
              el MSE
        """
        pred_train = np.dot(matriz_r, coeficientes)
        pred_test = np.dot(matriz_r_test, coeficientes)
        num_coefs = len(coeficientes)
        train_ccr = float(np.equal(np.round(pred_train), np.round(train_outputs)).sum()) / (len(train_outputs) * outputs) * 100
        test_ccr = float(np.equal(np.round(pred_test), np.round(test_outputs)).sum()) / (len(test_outputs) * outputs) * 100
        cmatrix = []
        errors = []
        train_mse = mean_squared_error(train_outputs, pred_train)
        test_mse = mean_squared_error(test_outputs, pred_test)  
        
    else:
        """
              Obtener las predicciones de entrenamiento y de test y calcular
              el CCR. Calcular también el MSE, comparando las probabilidades 
              obtenidas y las probabilidades objetivo
        """
        
        pred_train = logreg.predict(matriz_r)
        pred_train_prob = logreg.predict_proba(matriz_r)
        pred_test = logreg.predict(matriz_r_test)
        pred_test_prob = logreg.predict_proba(matriz_r_test)
        train_ccr = logreg.score(matriz_r, train_outputs) * 100
        test_ccr = logreg.score(matriz_r_test, test_outputs) * 100
        num_coefs = (abs(logreg.coef_) > 10e-5).sum()
        #print logreg.coef_.T
        cmatrix = confusion_matrix(test_outputs, pred_test)
        errors = np.nonzero(test_outputs.T[0] != pred_test)
        errors_pred_cls = pred_test[test_outputs.T[0] != pred_test]
        errors = np.vstack((errors, errors_pred_cls)).astype(int).T
        
        n_classes = len(np.unique(train_outputs))

        train_outputs_onehot = np.zeros((len(train_outputs), n_classes))
        train_outputs_onehot[np.arange(len(train_outputs)), train_outputs.flatten().astype(int)] = 1
        train_mse = mean_squared_error(pred_train_prob, train_outputs_onehot)
        
        test_outputs_onehot = np.zeros((len(test_outputs), n_classes))
        test_outputs_onehot[np.arange(len(test_outputs)), test_outputs.flatten().astype(int)] = 1
        test_mse = mean_squared_error(pred_test_prob, test_outputs_onehot)
    

    return train_mse, test_mse, train_ccr, test_ccr, num_coefs, cmatrix, errors

    
def lectura_datos(fichero_train, fichero_test, n_salidas):
    """ Realiza la lectura de datos.
        Recibe los siguientes parámetros:
            - fichero_train: nombre del fichero de entrenamiento.
            - fichero_test: nombre del fichero de test.
            - n_salidas: número de salidas.
        Devuelve:
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - test_inputs: matriz con las variables de entrada de 
              test.
            - test_outputs: matriz con las variables de salida de 
              test.
    """

    train = pd.read_csv(fichero_train)
    test = pd.read_csv(fichero_test)

    train_array = train.values
    test_array = test.values

    train_inputs = train_array[:,:-n_salidas]
    train_outputs = train_array[:,-n_salidas:]
    test_inputs = test_array[:,:-n_salidas]
    test_outputs = test_array[:,-n_salidas:] 
    
    return train_inputs, train_outputs, test_inputs, test_outputs

def inicializar_centroides_clas(train_inputs, train_outputs, num_rbf):
    """ Inicializa los centroides para el caso de clasificación.
        Debe elegir, aprox., num_rbf/num_clases
        patrones por cada clase. Recibe los siguientes parámetros:
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - centroides: matriz con todos los centroides iniciales
                          (num_rbf x num_entradas).
    """
    
    # Partición estratificada
    sss = StratifiedShuffleSplit(n_splits=2, train_size=num_rbf, test_size=None)
    centroides_index, _ = sss.split(train_inputs, train_outputs)
    centroides_mask = np.zeros(len(train_inputs), dtype=np.bool8)
    centroides_mask[centroides_index[0]] = True
    centroides = train_inputs[centroides_mask]

    return centroides

def clustering(clasificacion, train_inputs, train_outputs, num_rbf):
    """ Realiza el proceso de clustering. En el caso de la clasificación, se
        deben escoger los centroides usando inicializar_centroides_clas()
        En el caso de la regresión, se escogen aleatoriamente.
        Recibe los siguientes parámetros:
            - clasificacion: True si el problema es de clasificacion.
            - train_inputs: matriz con las variables de entrada de 
              entrenamiento.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - kmedias: objeto de tipo sklearn.cluster.KMeans ya entrenado.
            - distancias: matriz (num_patrones x num_rbf) con la distancia 
              desde cada patrón hasta cada rbf.
            - centros: matriz (num_rbf x num_entradas) con los centroides 
              obtenidos tras el proceso de clustering.
    """
    if clasificacion:
        centros = inicializar_centroides_clas(train_inputs, train_outputs, num_rbf)
        kmedias = KMeans(n_clusters=num_rbf, init=centros, n_init=1, max_iter=500)
    else:
        kmedias = KMeans(n_clusters=num_rbf, init='random', n_init=1, max_iter=500)
        
    distancias = kmedias.fit_transform(train_inputs)
    centros = kmedias.cluster_centers_

    return kmedias, distancias, centros

def calcular_radios(centros, num_rbf):
    """ Calcula el valor de los radios tras el clustering.
        Recibe los siguientes parámetros:
            - centros: conjunto de centroides.
            - num_rbf: número de neuronas de tipo RBF.
        Devuelve:
            - radios: vector (num_rbf) con el radio de cada RBF.
    """

    distancias = pdist(centros)
    distancias_sq = squareform(distancias)
    suma = distancias_sq.sum(axis=1)

    radios = suma / (2 * (num_rbf - 1))

    return radios

def calcular_matriz_r(distancias, radios):
    """ Devuelve el valor de activación de cada neurona para cada patrón 
        (matriz R en la presentación)
        Recibe los siguientes parámetros:
            - distancias: matriz (num_patrones x num_rbf) con la distancia 
              desde cada patrón hasta cada rbf.
            - radios: array (num_rbf) con el radio de cada RBF.
        Devuelve:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
    """

    matriz_r = np.exp(-np.square(distancias) / (2 * np.square(radios)))
    # Columna de sesgos
    sesgo = np.ones((distancias.shape[0], 1))
    # Añadir columna a la matriz
    matriz_r = np.append(matriz_r, sesgo, 1)

    return matriz_r

def invertir_matriz_regresion(matriz_r, train_outputs):
    """ Devuelve el vector de coeficientes obtenidos para el caso de la 
        regresión (matriz beta en las diapositivas)
        Recibe los siguientes parámetros:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
        Devuelve:
            - coeficientes: vector (num_rbf+1) con el valor del sesgo y del 
              coeficiente de salida para cada rbf.
    """
    
    if matriz_r.shape[0] >= matriz_r.shape[1]:
        inv_r = np.linalg.pinv(matriz_r)
    else: # Sin solución única
        print("No se puede obtener la inversa\n")
        return np.array([])

    coeficientes = np.dot(inv_r, train_outputs)

    return coeficientes

def logreg_clasificacion(matriz_r, train_outputs, eta, l2):
    """ Devuelve el objeto de tipo regresión logística obtenido a partir de la
        matriz R.
        Recibe los siguientes parámetros:
            - matriz_r: matriz (num_patrones x (num_rbf+1)) con el valor de 
              activación (out) de cada RBF para cada patrón. Además, añadimos
              al final, en la última columna, un vector con todos los 
              valores a 1, que actuará como sesgo.
            - train_outputs: matriz con las variables de salida de 
              entrenamiento.
            - eta: valor del parámetro de regularización para la Regresión 
              Logística.
            - l2: True si queremos utilizar L2 para la Regresión Logística. 
              False si queremos usar L1.
        Devuelve:
            - logreg: objeto de tipo sklearn.linear_model.LogisticRegression ya
              entrenado.
    """

    if l2:
        norm = 'l2'
    else:
        norm = 'l1'
    
    c = 1.0 / eta
    
    logreg = LogisticRegression(penalty=norm, C=c, fit_intercept=False).fit(matriz_r, train_outputs)
    
    return logreg


@cli.command('experimentar', help="Buscar la mejor configuracion")
def experiment():
    train_files_r = ['basesDatosPr3IMC/csv/train_forest.csv', 'basesDatosPr3IMC/csv/train_parkinsons.csv',
                     'basesDatosPr3IMC/csv/train_sin.csv']
    test_files_r =  ['basesDatosPr3IMC/csv/test_forest.csv', 'basesDatosPr3IMC/csv/test_parkinsons.csv',
                     'basesDatosPr3IMC/csv/test_sin.csv']
    train_files_c = ['basesDatosPr3IMC/csv/train_iris.csv', 'basesDatosPr3IMC/csv/train_nomnist.csv']
    test_files_c = ['basesDatosPr3IMC/csv/test_iris.csv', 'basesDatosPr3IMC/csv/test_nomnist.csv']
    
    ratios = [0.03, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
    first_eta = 1e-5
    first_l2 = False
    
    etas = [1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10]
    
    best_ratios = {}   
    best_values = {}
    
    s = "################################## EXPERIMENTO 1 ##################################";
    print('#' * len(s))
    print(s)
    print('#' * len(s))
    
    summary = 'BD & Ratio & Train MSE & Test MSE & N. Coef\\\\\\hline\n'

    for train, test, outputs in zip(train_files_r, test_files_r, [1, 2, 1]):
        for ratio in ratios:
            s = "================ " + train + " con ratio " + str(ratio) + " (Regresion) ================"
            print('=' * len(s))
            print(s)
            print('=' * len(s))
            train_mse, test_mse, train_ccr, test_ccr, num_coefs = entrenar_rbf_total(train, test, False, ratio, first_l2, first_eta, outputs)
            
            if not train in best_values or test_mse <= best_values[train]:
                best_values[train] = test_mse
                best_ratios[train] = ratio
            summary += '{} & {} & {} & {} & {}\\\\\\hline\n'.format(train, ratio, train_mse, test_mse, num_coefs)
        
        
    summary += 'BD & Ratio & Train MSE & Test MSE & Train CCR & Test CCR & N. Coef\\\\\\hline\n'
    
    
    for train, test in zip(train_files_c, test_files_c):
        for ratio in ratios:
            s = "================ " + train + " con ratio " + str(ratio) + " (Clasificacion) ================"
            print('=' * len(s))
            print(s)
            print('=' * len(s))
            train_mse, test_mse, train_ccr, test_ccr, num_coefs = entrenar_rbf_total(train, test, True, ratio, first_l2, first_eta, 1)
            
            if not train in best_values or test_ccr > best_values[train]:
                best_values[train] = test_ccr
                best_ratios[train] = ratio
            summary += '{} & {} & {} & {} & {} & {} & {}\\\\\\hline\n'.format(
                train, ratio, train_mse, test_mse, train_ccr, test_ccr, num_coefs)
                
    
    s = "########################## RESULTADOS EXPERIMENTO 1 ##########################";
    print('#' * len(s))
    print(s)
    print('#' * len(s))
    
    print(summary)    
    
    for train in train_files_r:
        print(("..." + train[-30:] if len(train) > 30 else train) + "\t\tMejor ratio: " + str(best_ratios[train]) + "\tMejor MSE: " + str(best_values[train]))
    for train in train_files_c:
        print(("..." + train[-30:] if len(train) > 30 else train) + "\t\tMejor ratio: " + str(best_ratios[train]) + "\tMejor CCR: " + str(best_values[train]))
            
            
    s = "################################## EXPERIMENTO 2 ##################################";
    print('#' * len(s))
    print(s)
    print('#' * len(s))
    
    best_etas = {}
    best_norms = {}
    best_values_2 = {}
    
    summary = 'BD & $\eta$ & L2 & Train MSE & Test MSE & Train CCR & Test CCR & N. Coef\\\\\\hline\n'

    for train, test in zip(train_files_c, test_files_c):
        for eta in etas:
            for norm in [False, True]:
                s = "================ " + train + " con eta " + str(eta) + " y " + ('L2' if norm else 'L1') + " (Clasificacion) ================"
                print('=' * len(s))
                print(s)
                print('=' * len(s))
                train_mse, test_mse, train_ccr, test_ccr, num_coefs = entrenar_rbf_total(train, test, True, best_ratios[train], norm, eta, 1)
                
                if not train in best_values_2 or test_ccr > best_values_2[train]:
                    best_values_2[train] = test_ccr
                    best_etas[train] = eta
                    best_norms[train] = norm
                summary += '{} & {} & {} & {} & {} & {} & {} & {}\\\\\\hline\n'.format(
                    train, eta, norm, train_mse, test_mse, train_ccr, test_ccr, num_coefs)

    s = "########################## RESULTADOS EXPERIMENTO 2 ##########################";
    print('#' * len(s))
    print(s)
    print('#' * len(s))
    
    print(summary)    
    
    for train in train_files_c:
        print(("..." + train[-30:] if len(train) > 30 else train) + "\t\tMejor eta: " + str(best_etas[train])+  "\t\tMejor norm: " + 'L2' if norm else 'L1' + "\tMejor CCR: " + str(best_values_2[train]))
    


if __name__ == "__main__":
    cli()
