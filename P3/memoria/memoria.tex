\documentclass[12pt, a4paper, oneside, titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish, es-tabla]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage{ragged2e}
\usepackage[]{tocbibind}
\usepackage{float}
\usepackage{placeins}
\usepackage{graphics}
\usepackage{color}


\author{Víctor Manuel Vargas Yun}
\title{Introducción a los modelos computacionales: Práctica 3}
\date{Curso académico 2017-2018\\Córdoba, \today}

\newcommand{\myparagraph}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\mysubparagraph}[1]{\subparagraph{#1}\mbox{}\\}

\renewcommand{\listalgorithmname}{Índice de algoritmos}

\begin{document}
\begin{titlepage}
\centering
\includegraphics[width=0.20\textwidth]{uco.png}
\includegraphics[width=0.18\textwidth]{eps.png}\par\vspace{1cm}
{\scshape\LARGE Universidad de Córdoba\par}
{\scshape\Large Escuela Politécnica Superior de Córdoba\par}
\vspace{1cm}
{\scshape\LARGE Ingeniería Informática\par}
{\scshape\Large Especialidad: Computación\par}
{\scshape\Large Cuarto curso. Primer cuatrimestre\par}
\vspace{1.5cm}
{\scshape\LARGE Introducción a los modelos computacionales.\par}
\vspace{1.5cm}
{\huge\bfseries Práctica 3: Redes neuronales de funciones de base radial.\par}
\vspace{1.2cm}
{\Large\itshape Víctor Manuel Vargas Yun\par}
20620656Y\par
i42vayuv@uco.es\par
\vfill

{\large Curso académico 2017-2018\\Córdoba, \today\par}
\end{titlepage}

\pagenumbering{roman}

\tableofcontents
\newpage

\listoffigures
\newpage

\listoftables
\newpage

\clearpage
\pagenumbering{arabic}

\def\BState{\State\hskip-\ALG@thistlm}

\setlength{\parskip}{0.7em}

\section{Entrenamiento de una red RBF}
El entrenamiento de las redes RBF se realiza en tres pasos principales: un clustering, una heurística simple y el cálculo de una pseudo-inversa o una regresión logística.

\subsection{Determinación de los centros}
En primer lugar se utiliza un algoritmo de clustering para determinar cuáles serán los centros de las funciones RBF, que son los pesos desde la capa de entrada hacia la capa oculta. En los problemas de clasificación, se realizará seleccionando aleatoriamente $n_1 / k$ patrones de cada clase, siendo $n_1$ el número de clusters y $k$ el número de clases. En los problemas de regresión, simplemente se seleccionan $n_1$ patrones aleatoriamente.

Para realizar la selección estratificada de patrones para clasificación, se ha utilizado una clase de \texttt{scikit-learn} llamada \texttt{StratifiedShuffleSplit}.

Una vez inicializados los centroides, se ejecuta el algoritmo de clustering. En este caso se utiliza \emph{KMeans} que está implementado también en \texttt{scikit-learn}. Este algoritmo recibe como parámetro los centroides iniciales, en el caso de clasificación. En el caso de regresión, se indica que la inicialización se haga de forma aleatoria. También se ha indicado que esta inicialización se haga solo una vez.

\subsection{Cálculo de los radios de cada RBF}
El cálculo de los radios de las RBF se hace mediante una heurística simple. Su valor será igual a la mitad de la media de las distancias desde esa RBF a las demás RBFs. La expresión del radio se puede escribir de la siguiente forma:

\begin{equation}
\sigma_j = \frac{1}{2(n_1 - 1)} \sum_{i \neq j} ||c_j - c_i||
\end{equation}

\subsection{Ajuste de los pesos de capa oculta a capa de salida}
Por último, se deben actualizar los pesos de capa oculta a capa de salida. Para ello, dependiendo de si se trata de regresión o clasificación, se utilizará un método distinto.

\begin{itemize}
	\item Para los problemas de regresión, la actualización de pesos se realizará mediante la matriz pseudo-inversa de Moore Penrose. La matriz de coeficientes se calcularía de la siguiente forma:
	
	\begin{equation}
		\beta^T_{((n_1+1) \times k)} = (R^+)_{((n_1+1) \times N)} Y_{(N \times k)}
	\end{equation}
	
	siendo $\beta$ la matriz de coeficientes, R la matriz que contiene las salidas de las RBF e Y la matriz que contiene las salidas esperadas.
	
	\item Para los problemas de clasificación, se utilizará una regresión logística para hacer la actualización de pesos. Cabe destacar la importancia de un buen ajuste del parámetro C para lograr mejores resultados. Este parámetro determina la importancia del error de aproximación frente al error de regularización y su valor es  $C = 1/\eta$.
\end{itemize}

\section{Experimentos y análisis de resultados}
\subsection{Bases de datos utilizadas}
A continuación se realizará una breve descripción de las bases de datos que se han utilizado para ejecutar los experimentos. Se han utilizado dos para clasificación y tres para regresión.

Las bases de datos de regresión son las siguientes:
\begin{itemize}
	\item \emph{Función seno}: está compuesta por 120 patrones de entrenamiento y 41 de \emph{test}. Se ha obtenido a partir de una función seno a la que se le ha añadido cierto ruido.
	\item \emph{Base de datos forest}: está compuesta por 387 patrones de entrenamiento y 130 de \emph{test}. Sus entradas son una serie de parámetros meteorológicos y otras variables descriptoras sobre incendios en bosques al norte de Portugal. Sus salidas son el área quemada.
	\item \emph{Base de datos parkinsons}: está compuesta por 4406 patrones de entrenamiento y 1469 de generalización. Sus entradas son uan serie de datos clínicos de pacientes con la enfermedad Parkinson y datos de medidas biométricas de la voz. Como salidas tiene el valor motor y total del UPDRS.
\end{itemize}

Para clasificación, se han utilizado las siguientes bases de datos:
\begin{itemize}
	\item \emph{Base de datos iris}: contiene 112 patrones de \emph{train} y 38 patrones de \emph{test}. Los parámetros de entradas son el ancho y largo del pétalo y del sépalo. Las clases son las diferentes especies de la flor \emph{iris}, que son: \emph{iris versicolor}, \emph{iris setosa} e \emph{iris virginica}.
	\item \emph{Base de datos noMNIST}: se ha utilizado una versión reducida de la base de datos original \emph{noMNIST} que contiene 900 patrones de entrenamiento y 300 de generalización. Los patrones consisten en diferentes imágenes que representan letras de la A a la F escritas con diferentes tipografías. Las letras están ajustadas en una rejilla cuadrada de 28 x 28 píxeles. Todas ellas están en escala de grises y normalizadas entre $[-1;1]$. Existen 6 clases de salida que se corresponden con las diferentes letras a, b, c, d, e y f.
\end{itemize}

\subsection{Parámetros considerados}
A continuación se describen los principales parámetros que se han considerado a la hora de hacer los experimentos.

\begin{itemize}
	\item \emph{Parámetro eta} ($\eta$): se utiliza para determinar el valor del parámetro C que se utiliza en la regresión logística para los problemas de clasificación ($C = 1 / \eta$).
	\item \emph{Ratio rbf}: determina el número de neuronas RBF que se van a utilizar. El valor debe estar en el intervalo $(0,1)$. El número de neuronas RBF será el número de patrones multiplicado por el \emph{ratio rbf}.
	\item Tipo de regularización: se puede indicar si utilizará regularización L1 o L2 para la regresión logística en los problemas de clasificación.
\end{itemize}

Como se puede observar, existe un mayor número de parámetros para los problemas de clasificación que para los de regresión.

\subsection{Experimento 1: Ajuste del Ratio RBF}
El primer experimento se ha realizado con el objetivo de determinar cuál es el mejor ratio RBF para cada una de las bases de datos. Este es un parámetro que afecta tanto a clasificación como a regresión, por lo que se ha realizado con todas las bases de datos.

La Tabla \ref{table:Experimento1Reg} muestra los resultados del primer experimento en el que se ha variado el \emph{Ratio RBF}, dándole los valores: 0.03, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45 y 0.5.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{ c | c | c | c | c }
		BD & Ratio & Train MSE & Test MSE & N. Coef\\\hline
		{\color{red} \textbf{forest}} & {\color{red} $0.03$} & {\color{red} $0.00154777900644$} & {\color{red} $0.00909585058852$} & {\color{red} $13.0$}\\\hline
		forest & $0.05$ & $0.0015227679902$ & $0.00909625509022$ & $20.0$\\\hline
		forest & $0.1$ & $0.00141030466498$ & $0.00912467949839$ & $40.0$\\\hline
		forest & $0.15$ & $0.0013973310532$ & $0.0091650367837$ & $59.0$\\\hline
		forest & $0.2$ & $0.00131171744231$ & $0.00925708063239$ & $78.0$\\\hline
		forest & $0.25$ & $0.0012554492616$ & $0.00924749358904$ & $98.0$\\\hline
		forest & $0.3$ & $0.00122899130254$ & $0.00926206753$ & $117.0$\\\hline
		forest & $0.35$ & $0.00116866276088$ & $0.00934143877073$ & $136.0$\\\hline
		forest & $0.4$ & $0.00112940204343$ & $0.00940476983763$ & $155.0$\\\hline
		forest & $0.45$ & $0.0010818813744$ & $0.00943265991213$ & $175.0$\\\hline
		forest & $0.5$ & $0.00102770076158$ & $0.00974250105091$ & $194.0$\\\hline
		parkinsons & $0.03$ & $0.0253433078444$ & $0.0275802767385$ & $133.0$\\\hline
		parkinsons & $0.05$ & $0.0215775238495$ & $0.0248560695776$ & $221.0$\\\hline
		parkinsons & $0.1$ & $0.0167461893933$ & $0.0210670084576$ & $442.0$\\\hline
		parkinsons & $0.15$ & $0.014094747583$ & $0.0201443051806$ & $662.0$\\\hline
		{\color{red} \textbf{parkinsons}} & {\color{red} $0.2$} & {\color{red} $0.012125068989$} & {\color{red} $0.0192887704653$} & {\color{red} $882.0$}\\\hline
		parkinsons & $0.25$ & $0.0105491616267$ & $0.0205374732006$ & $1102.0$\\\hline
		parkinsons & $0.3$ & $0.00911586144569$ & $0.0250791022636$ & $1323.0$\\\hline
		parkinsons & $0.35$ & $0.00791097854592$ & $0.0285000479946$ & $1543.0$\\\hline
		parkinsons & $0.4$ & $0.00687786132646$ & $0.0358316747388$ & $1763.0$\\\hline
		parkinsons & $0.45$ & $0.00603483932275$ & $0.0474815142244$ & $1983.0$\\\hline
		parkinsons & $0.5$ & $0.00523919436202$ & $0.062909342721$ & $2204.0$\\\hline
		sin & $0.03$ & $0.0292614276384$ & $0.0394116536837$ & $5.0$\\\hline
		{\color{red} \textbf{sin}} & {\color{red} $0.05$} & {\color{red} $0.0140419174475$} & {\color{red} $0.0208528857533$} & {\color{red} $7.0$}\\\hline
		sin & $0.1$ & $0.0125867154851$ & $0.025992001407$ & $13.0$\\\hline
		sin & $0.15$ & $0.011299736481$ & $0.307144006061$ & $19.0$\\\hline
		sin & $0.2$ & $0.0104719184673$ & $1.21557937263$ & $25.0$\\\hline
		sin & $0.25$ & $0.0103083300686$ & $2.06031562488$ & $31.0$\\\hline
		sin & $0.3$ & $0.0104094164585$ & $1.73949895038$ & $37.0$\\\hline
		sin & $0.35$ & $0.0104119770894$ & $1.84018006635$ & $43.0$\\\hline
		sin & $0.4$ & $0.0104142932483$ & $1.78998753435$ & $49.0$\\\hline
		sin & $0.45$ & $0.0104141915529$ & $1.68869432927$ & $55.0$\\\hline
		sin & $0.5$ & $0.0104137178538$ & $1.6944401729$ & $61.0$\\\hline
	\end{tabular}
	\caption{Resultados del experimento 1 en regresión}
	\label{table:Experimento1Reg}
\end{table}

Si analizamos los datos, se pueden obtener algunas conclusiones:
\begin{itemize}
	\item La base de datos \emph{forest} ha obtenido los mejores resultados con el menor número de neuronas RBF. Al parecer, cuanto mayor sea el número de neuronas, mayor sobreentrenamiento realiza. Esto se puede ver en el hecho de que mientras mayor es el ratio, menor es el error de entrenamiento pero mayor es el de test. En la Figura \ref{fig:Exp1ForestMSE} se ha representado gráficamente este comportamiento.
	
	\begin{figure}[!htbp]
		\centering
		\includegraphics[scale=0.75]{1_forest_mse.png}
		\caption{Ratio RBF vs Errores en \emph{Forest}}
		\label{fig:Exp1ForestMSE}
	\end{figure}
	
	Debido a este sobreentrenamiento, no se han obtenido resultados tan buenos como se podía esperar.
	
	\item La base de datos \emph{parkinsons} obtiene su mejor valor de error de \emph{test} con el ratio $0.2$. A partir de este valor, el error de entrenamiento continúa disminuyendo pero el de \emph{test} aumenta según aumenta el valor del ratio. Es un claro caso de sobreajuste. Es muy probable que en esta base de datos se haya podido utilizar un ratio mayor porque el número de patrones también es mayor y se tarda más en llegar al punto de sobreentrenar. En la figura \ref{fig:Exp1ParkinsonsMSE} se puede ver claramente el punto en el que empieza a sobreentrenar.
	
	\begin{figure}[!htbp]
		\centering
		\includegraphics[scale=0.75]{1_parkinsons_mse.png}
		\caption{Ratio RBF vs Errores en \emph{Parkinsons}}
		\label{fig:Exp1ParkinsonsMSE}
	\end{figure}
	
	\item Por último, en el caso de la base de datos \emph{sin}, se alcanza el punto óptimo en el ratio $0.05$ que es uno de los más pequeños que se ha probado. Parece extraño que \emph{sin} haya funcionado bien con $0.05$ mientras que \emph{forest}, que tiene más patrones, ha funcionado mejor con $0.03$. Esto se puede deber a las características de los patrones de ambas bases de datos. Si los patrones son más parecidos entre sí, es más fácil que el modelo sobreajuste que si los patrones son más diferentes. También se debe tener en cuenta que el ratio es dependiente del número de patrones (el número de neuronas RBF es un porcentaje del total de patrones). La Figura \ref{fig:Exp1SinMSE} muestra ambos errores representados gráficamente.
	
	\begin{figure}[!htbp]
		\centering
		\includegraphics[scale=0.75]{1_sin_mse.png}
		\caption{Ratio RBF vs Errores en \emph{Sin}}
		\label{fig:Exp1SinMSE}
	\end{figure}
	
\end{itemize}

\FloatBarrier

Por otro lado, la Tabla \ref{table:Experimento1Clas} muestra los resultados del mismo experimento pero realizado sobre las dos bases de datos de clasificación.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{ c | c | c | c | c | c | c }
		BD & Ratio & Train MSE & Test MSE & Train CCR & Test CCR & N. Coef\\\hline	
		iris & $0.03$ & $0.0250585252135$ & $0.0295841746465$ & $96.3963963964$ & $89.1891891892$ & $12.0$\\\hline
		iris & $0.05$ & $0.0146547087529$ & $0.03385993674$ & $96.9369369369$ & $91.8918918919$ & $21.0$\\\hline
		{\color{red} \textbf{iris}} & {\color{red} $0.1$} & {\color{red} $0.00463786640761$} & {\color{red} $0.0305959751051$} & {\color{red} $99.2792792793$} & {\color{red} $94.5945945946$} & {\color{red} $36.0$}\\\hline
		iris & $0.15$ & $0.00105251573033$ & $0.0453911222986$ & $100.0$ & $91.8918918919$ & $54.0$\\\hline
		iris & $0.2$ & $0.000286845142811$ & $0.0555632035392$ & $100.0$ & $91.3513513514$ & $68.8$\\\hline
		iris & $0.25$ & $0.000290219583897$ & $0.0559063863412$ & $100.0$ & $91.3513513514$ & $86.6$\\\hline
		iris & $0.3$ & $0.000303614786668$ & $0.0604920473265$ & $100.0$ & $90.2702702703$ & $101.8$\\\hline
		iris & $0.35$ & $0.000170108368675$ & $0.0655294901788$ & $100.0$ & $89.7297297297$ & $118.8$\\\hline
		iris & $0.4$ & $0.000125910276401$ & $0.0673254889459$ & $100.0$ & $89.1891891892$ & $132.8$\\\hline
		iris & $0.45$ & $9.07478970262e-05$ & $0.067755598885$ & $100.0$ & $89.7297297297$ & $149.2$\\\hline
		iris & $0.5$ & $9.18805692813e-05$ & $0.0669240321762$ & $100.0$ & $89.7297297297$ & $166.2$\\\hline
		nomnist & $0.03$ & $0.0383977189049$ & $0.0355206129563$ & $83.9377085651$ & $87.1571906355$ & $168.0$\\\hline
		nomnist & $0.05$ & $0.0288496378644$ & $0.0306868985149$ & $88.2313681869$ & $87.6254180602$ & $276.0$\\\hline
		nomnist & $0.1$ & $0.0120445949953$ & $0.0378943312463$ & $96.0622914349$ & $86.6889632107$ & $546.0$\\\hline
		nomnist & $0.15$ & $0.000557949586849$ & $0.04397179787$ & $99.977753059$ & $86.6220735786$ & $816.0$\\\hline
		nomnist & $0.2$ & $6.31536276237e-05$ & $0.0399636708704$ & $100.0$ & $86.9565217391$ & $1086.0$\\\hline
		nomnist & $0.25$ & $3.14214598536e-05$ & $0.0376649677081$ & $100.0$ & $87.6254180602$ & $1356.0$\\\hline
		nomnist & $0.3$ & $2.58008846644e-05$ & $0.0392774105947$ & $100.0$ & $86.889632107$ & $1626.0$\\\hline
		nomnist & $0.35$ & $2.12073984399e-05$ & $0.0377529292559$ & $100.0$ & $87.2240802676$ & $1896.0$\\\hline
		{\color{red} \textbf{nomnist}} & {\color{red} $0.4$} & {\color{red} $1.87162169675e-05$} & {\color{red} $0.0361586088386$} & {\color{red} $100.0$} & {\color{red} $88.0267558528$} & {\color{red} $2166.0$}\\\hline
		nomnist & $0.45$ & $1.8420222465e-05$ & $0.0350056183285$ & $100.0$ & $87.7591973244$ & $2436.0$\\\hline
		nomnist & $0.5$ & $1.51594052804e-05$ & $0.0352138462058$ & $100.0$ & $87.6254180602$ & $2705.8$\\\hline
		
	\end{tabular}
	}
	\caption{Resultados del experimento 1 en clasificación}
	\label{table:Experimento1Clas}
\end{table}

Si analizamos los datos, podemos obtener las siguientes conclusiones:
\begin{itemize}
	\item El mejor valor para el ratio en la base de datos \emph{iris} ha sido $0.1$. A partir de ese momento, se puede decir que el modelo se ha ajustado totalmente a los datos del conjunto de entrenamiento ya que el error pasa a ser cero y el CCR pasa a ser el $100\%$. Esto no es bueno ya que pierde capacidad de generalización. La Figura \ref{fig:Exp1IrisCCR} muestra claramente esta tendencia.
	
	\begin{figure}[!htbp]
		\centering
		\includegraphics[scale=0.75]{1_iris_ccr.png}
		\caption{Ratio RBF vs CCR en \emph{Iris}}
		\label{fig:Exp1IrisCCR}
	\end{figure}
	
	\item La base de datos \emph{noMNIST}, que es bastante compleja, ha funcionado mejor con un porcentaje bastante alto de neuronas RBF ($40\%$). A diferencia del caso anterior, a pesar de haber conseguido un CCR de \emph{train} del $100\%$ desde el ratio $0.2$, el error y el CCR de \emph{test} continúan mejorando hasta llegar al ratio $0.4$. A partir de ese punto, el CCR de entrenamiento se mantiene en el $100\%$ pero el de \emph{test} empieza a disminuir. En la Figura \ref{fig:Exp1NomnistCCR} se puede observar que el comportamiento no es tan claro como en los casos anteriores.
	
	\begin{figure}[!htbp]
		\centering
		\includegraphics[scale=0.75]{1_nomnist_ccr.png}
		\caption{Ratio RBF vs CCR en \emph{NoMNIST}}
		\label{fig:Exp1NomnistCCR}
	\end{figure}
	
\end{itemize}

\FloatBarrier

\subsection{Experimento 2: Determinación de eta y tipo de normalización}
El segundo experimento ha tratado de determinar los mejores valores para el parámetro eta y el mejor tipo de regularización. Estos parámetros afectan solamente a los problemas de clasificación, por lo que se ha realizado solo con las bases de datos \emph{iris} y \emph{noMNIST}.

Se han probado los siguientes valores de eta: 1, 0.1, 0.01, 0.001, 0.0001, $10^{-5}$, $10^{-6}$, $10^{-7}$, $10^{-8}$, $10^{-9}$ y $10^{-10}$. Para la regularización, se han probado las dos alternativas que tenemos disponibles: L1 y L2.

La Tabla \ref{table:Experimento2Iris} muestra los resultados del experimento para la base de datos \emph{iris}.
	
\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{ c | c | c | c | c | c | c | c }
			BD & $\eta$ & Reg & Train MSE & Test MSE & Train CCR & Test CCR & N. Coef\\\hline
			iris & $1$ & L1 & $0.0271282584592$ & $0.0220640306385$ & $96.2162162162$ & $98.3783783784$ & $17.8$\\\hline
			iris & $1$ & L2 & $0.0372401602345$ & $0.0257889038732$ & $93.8738738739$ & $96.7567567568$ & $36.0$\\\hline
			iris & $0.1$ & L1 & $0.0124347430064$ & $0.0149993165909$ & $96.7567567568$ & $95.6756756757$ & $21.4$\\\hline
			{\color{red} \textbf{iris}} & {\color{red} $0.1$} & {\color{red} L2} & {\color{red} $0.0169963129508$} & {\color{red} $0.0122028502248$} & {\color{red} $96.2162162162$} & {\color{red} $98.9189189189$} & {\color{red} $36.0$}\\\hline
			iris & $0.01$ & L1 & $0.0111199835045$ & $0.0207353374876$ & $98.1981981982$ & $94.5945945946$ & $26.0$\\\hline
			iris & $0.01$ & L2 & $0.0123967969974$ & $0.0146766485795$ & $96.5765765766$ & $95.6756756757$ & $36.0$\\\hline
			iris & $0.001$ & L1 & $0.00707613158042$ & $0.0304545305709$ & $98.3783783784$ & $93.5135135135$ & $31.4$\\\hline
			iris & $0.001$ & L2 & $0.0114334346128$ & $0.0203513147227$ & $97.6576576577$ & $94.5945945946$ & $36.0$\\\hline
			iris & $0.0001$ & L1 & $0.00499577958009$ & $0.0303368160948$ & $98.9189189189$ & $94.5945945946$ & $35.2$\\\hline
			iris & $0.0001$ & L2 & $0.0105957563533$ & $0.026296162971$ & $98.018018018$ & $94.0540540541$ & $36.0$\\\hline
			iris & $1e-05$ & L1 & $0.00463786640761$ & $0.0305959751051$ & $99.2792792793$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-05$ & L2 & $0.00757699769715$ & $0.0363664220761$ & $98.1981981982$ & $92.972972973$ & $36.0$\\\hline
			iris & $1e-06$ & L1 & $0.00501101402243$ & $0.0311727096032$ & $99.0990990991$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-06$ & L2 & $0.00389897322465$ & $0.0381820041353$ & $99.2792792793$ & $92.972972973$ & $36.0$\\\hline
			iris & $1e-07$ & L1 & $0.00529744947687$ & $0.0312981090709$ & $98.9189189189$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-07$ & L2 & $0.00264395605343$ & $0.0383592049165$ & $99.6396396396$ & $93.5135135135$ & $36.0$\\\hline
			iris & $1e-08$ & L1 & $0.00538862429449$ & $0.0314583692244$ & $98.9189189189$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-08$ & L2 & $0.00269381200116$ & $0.0380945871291$ & $99.4594594595$ & $93.5135135135$ & $36.0$\\\hline
			iris & $1e-09$ & L1 & $0.00538860191455$ & $0.031458378183$ & $98.9189189189$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-09$ & L2 & $0.00243043244654$ & $0.0406892748696$ & $99.6396396396$ & $92.972972973$ & $36.0$\\\hline
			iris & $1e-10$ & L1 & $0.00538859967659$ & $0.0314583790785$ & $98.9189189189$ & $94.5945945946$ & $36.0$\\\hline
			iris & $1e-10$ & L2 & $0.00243321186693$ & $0.0407660542707$ & $99.6396396396$ & $92.4324324324$ & $36.0$\\\hline
		\end{tabular}
	}
	\caption{Resultados del experimento 2 para \emph{iris}}
	\label{table:Experimento2Iris}
\end{table}

Si observamos los datos, se pueden extraer las siguientes conclusiones:
\begin{itemize}
	\item La regularización L2 obtiene mejores resultados en test que la L1 aunque el tiempo que tarda en entrenar es mayor debido a que el número de coeficientes es mayor también. Sin embargo, la L1 obtiene mejores resultados en \emph{train}. Aquí se puede apreciar el efecto que tiene la regularización sobre el entrenamiento: es una forma de evitar el sobreajuste.
	\item El mejor resultado se ha obtenido con la regularización L2 y con el valor $0.1$ para el parámetro eta. A partir de este valor, conforme se disminuye el parámetro, aumenta el CCR de \emph{train} pero disminuye el de \emph{test}. Con valores bastante pequeños de eta, se obtiene un CCR de entrenamiento de casi el $100\%$ pero el CCR de generalización obtenido es bastante malo.
\end{itemize}

\FloatBarrier

La Tabla \ref{table:Experimento2NoMNIST} muestra los resultados del experimento para la base de datos \emph{noMNIST}.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{ c | c | c | c | c | c | c | c }
			BD & $\eta$ & Reg & Train MSE & Test MSE & Train CCR & Test CCR & N. Coef\\\hline
			nomnist & $1$ & L1 & $0.0428153318145$ & $0.0398072182301$ & $85.9176863181$ & $89.2307692308$ & $194.8$\\\hline
			nomnist & $1$ & L2 & $0.037661390035$ & $0.0358091848199$ & $87.8309232481$ & $90.5685618729$ & $2165.4$\\\hline
			nomnist & $0.1$ & L1 & $0.0130235935117$ & $0.024437900073$ & $96.2625139043$ & $90.7692307692$ & $593.6$\\\hline
			{\color{red} \textbf{nomnist}} & {\color{red} $0.1$} & {\color{red} L2} & {\color{red} $0.0203054176355$} & {\color{red} $0.024748634002$} & {\color{red} $94.0155728587$} & {\color{red} $91.5050167224$} & {\color{red} $2166.0$}\\\hline
			nomnist & $0.01$ & L1 & $0.00111815151346$ & $0.03218435634$ & $100.0$ & $88.4949832776$ & $926.6$\\\hline
			nomnist & $0.01$ & L2 & $0.00851614660715$ & $0.0248688224593$ & $97.8420467186$ & $90.1003344482$ & $2166.0$\\\hline
			nomnist & $0.001$ & L1 & $6.78935844218e-05$ & $0.0351288747658$ & $100.0$ & $88.0267558528$ & $1740.8$\\\hline
			nomnist & $0.001$ & L2 & $0.00137029286328$ & $0.0310689607054$ & $99.977753059$ & $88.8294314381$ & $2166.0$\\\hline
			nomnist & $0.0001$ & L1 & $2.17730231568e-05$ & $0.0360478382929$ & $100.0$ & $87.8929765886$ & $2159.4$\\\hline
			nomnist & $0.0001$ & L2 & $7.59054486117e-05$ & $0.0355859758617$ & $100.0$ & $87.9598662207$ & $2166.0$\\\hline
			nomnist & $10^{-05}$ & L1 & $1.87162169675e-05$ & $0.0361586088386$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-05}$ & L2 & $3.00858110504e-06$ & $0.0381495186014$ & $100.0$ & $87.491638796$ & $2166.0$\\\hline
			nomnist & $10^{-06}$ & L1 & $1.74047284537e-05$ & $0.0362312319929$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-06}$ & L2 & $2.44310652371e-07$ & $0.0392194520462$ & $100.0$ & $87.4247491639$ & $2166.0$\\\hline
			nomnist & $10^{-07}$ & L1 & $1.73761911871e-05$ & $0.0362330559543$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-07}$ & L2 & $2.0543067443e-07$ & $0.0393844246584$ & $100.0$ & $87.2909698997$ & $2166.0$\\\hline
			nomnist & $10^{-08}$ & L1 & $1.73732772448e-05$ & $0.0362332408392$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-08}$ & L2 & $2.4579250042e-07$ & $0.0392333135883$ & $100.0$ & $87.3578595318$ & $2166.0$\\\hline
			nomnist & $10^{-09}$ & L1 & $1.73729858679e-05$ & $0.0362332593281$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-09}$ & L2 & $2.13264629108e-07$ & $0.0391675283663$ & $100.0$ & $87.2909698997$ & $2166.0$\\\hline
			nomnist & $10^{-10}$ & L1 & $1.73729567304e-05$ & $0.0362332611771$ & $100.0$ & $88.0267558528$ & $2166.0$\\\hline
			nomnist & $10^{-10}$ & L2 & $1.02027325221e-07$ & $0.0399206793505$ & $100.0$ & $87.0234113712$ & $2166.0$\\\hline
		\end{tabular}
	}
	\caption{Resultados del experimento 2 para \emph{noMNIST}}
	\label{table:Experimento2NoMNIST}
\end{table}

Observando los resultados, se pueden extraer algunas conclusiones:
\begin{itemize}
	\item De nuevo, los mejores resultados se han conseguido con L2 y $\eta = 0.1$.
	\item Si se disminuye el parámetro eta, el modelo empieza a sobreentrenar. Se consigue un CCR de entrenamiento del $100\%$ mientras que el de \emph{test} disminuye.
	\item La regularización L2 evita el sobreentrenamiento mejor que la L1. Esto se puede observar cuando se disminuye eta. Las ejecuciones con regularización L1 alcanzan antes el $100\%$ de CCR \emph{train} (que conlleva un menor CCR de \emph{test}).
	\item Una buena clasificación es bastante sensible al parámetro eta.
\end{itemize}

\FloatBarrier

\subsection{Resumen de los experimentos}
Con el objetivo de agrupar las mejores configuraciones para cada una de las bases de datos, se resumen a continuación:

\begin{itemize}
	\item \emph{forest}: ratio $0.03$.
	\item \emph{parkinsons}: ratio $0.2$.
	\item \emph{sin}: ratio $0.05$.
	\item \emph{iris}: ratio $0.1$, eta $0.1$ y regularización L2.
	\item \emph{noMNIST}: ratio $0.4$, eta $0.1$ y regularización L2.
\end{itemize}

\subsection{Ejecución de clasificación como regresión}
A continuación, utilizando la mejor configuración de \emph{iris} y \emph{noMNIST}, se realizará una ejecución indicando que la base de datos es de regresión, aunque en realidad se trate de clasificación.

Para la base de datos \emph{iris}, se obtienen los siguientes valores:
\begin{itemize}
	\item MSE de entrenamiento: $0.030972 \pm 0.001422$
	\item MSE de \emph{test}: $0.034014 \pm 0.003663$
	\item CCR de entrenamiento: $96.40\% \pm 0.57\%$
	\item CCR de \emph{test}: $95.14\% \pm 1.08\%$
\end{itemize}

Si se compara con los resultados que se obtuvieron anteriormente, se puede observar que el CCR de entrenamiento es ligeramente superior en este caso, pero el CCR de \emph{test} es bastante menor.

Por otro lado, para la base de datos \emph{noMNIST} se obtienen los siguientes resultados:
\begin{itemize}
	\item MSE de entrenamiento: $0.338211 \pm 0.027737$
	\item MSE de \emph{test}: $0.699394 \pm 0.014466$
	\item CCR de entrenamiento: $76.93\% \pm 1.57\%$
	\item CCR de \emph{test}: $64.55\% \pm 1.37\%$	
\end{itemize}

En este caso, tanto el CCR de entrenamiento como el de generalización son bastante menores.

Como conclusión, se puede ver que no merece la pena ejecutar los problemas de clasificación como si fueran de regresión. La única ventaja que se ha observado es que las ejecuciones en regresión son ligeramente más rápidas que las de clasificación.

\subsection{Matriz de confusión de noMNIST}
La Tabla \ref{table:MatrizConfusiónNomnist} muestra la matriz de confusión de la mejor configuración obtenida con \emph{noMNIST}. La clase real está representada en la parte superior y la parte predicha en la parte izquierda.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c c c c c c c}
		 & A & B & C & D & E & F\\
		A & 46 & 0 & 0 & 2 & 0 & 1\\
		B & 0 & 47 & 0 & 2 & 1 & 0\\
		C & 0 & 0 & 46 & 0 & 2 & 2\\
		D & 1 & 3 & 0 & 45 & 0 & 1\\
		E & 0 & 1 & 1 & 0 & 46 & 2\\
		F & 2 & 1 & 0 & 1 & 1 & 45
	\end{tabular}
	\caption{Matriz de confusión de la mejor configuración \emph{noMNIST}}
	\label{table:MatrizConfusiónNomnist}
\end{table}

\FloatBarrier

La Tabla \ref{table:MatrizConfusiónNomnistMLP} muestra la matriz de confusión de la mejor configuración obtenida con \emph{noMNIST} utilizando el MLP de la práctica anterior. La clase real está representada en la parte izquierda y la clase predicha en la parte superior.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c c c c c c c}
		&A&B&C&D&E&F\\
		A&46&1&0&2&0&1\\
		B&1&42&1&1&3&2\\
		C&1&1&44&0&2&2\\
		D&2&3&2&41&2&0\\
		E&3&0&1&0&41&5\\
		F&3&0&0&1&2&44
	\end{tabular}
	\caption{Matriz de confusión de la mejor configuración \emph{noMNIST}}
	\label{table:MatrizConfusiónNomnistMLP}
\end{table}

En el caso de la matriz del MLP se puede observar como el mayor número de errores se comete entre la E y la F mientras que en la matriz de la red RBF no se puede observar una tendencia tan clara. El mayor número de errores se ha cometido entre la B y la D, aunque el número de errores es bastante menor. También tiene sentido que la B y la D sean las letras que más se confunden ya que, al igual que la E y la F, se parecen bastante entre sí.

A continuación, se mostrarán ejemplos de patrones que han sido clasificados erróneamente por la red RBF.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{24_D.png}
	\includegraphics[scale=1]{27_D.png}
	\includegraphics[scale=1]{40_F.png}
	\caption{Patrones de A mal clasificados}
	\label{fig:PatronesMalA}
\end{figure}

La Figura \ref{fig:PatronesMalA} muestra algunos de los patrones de A que se han clasificado mal. El primer patrón ha sido clasificado como D, el segundo como D y el tercero como F.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{83_E.png}
	\includegraphics[scale=1]{90_D.png}
	\includegraphics[scale=1]{98_D.png}
	\caption{Patrones de B mal clasificados}
	\label{fig:PatronesMalB}
\end{figure}

La Figura \ref{fig:PatronesMalB} muestra algunos de los patrones de B que se han clasificado mal. El primer patrón ha sido clasificado como E, el segundo como D y el tercero como D.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{107_E.png}
	\includegraphics[scale=1]{139_F.png}
	\includegraphics[scale=1]{142_E.png}
	\caption{Patrones de C mal clasificados}
	\label{fig:PatronesMalC}
\end{figure}

La Figura \ref{fig:PatronesMalC} muestra algunos de los patrones de C que se han clasificado mal. El primer patrón ha sido clasificado como E, el segundo como F y el tercero como E.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{160_B.png}
	\includegraphics[scale=1]{176_B.png}
	\includegraphics[scale=1]{180_B.png}
	\caption{Patrones de D mal clasificados}
	\label{fig:PatronesMalD}
\end{figure}

La Figura \ref{fig:PatronesMalD} muestra algunos de los patrones de D que se han clasificado mal. Todos ellos han sido clasificados como B.
	
	
\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{206_B.png}
	\includegraphics[scale=1]{215_F.png}
	\includegraphics[scale=1]{222_F.png}
	\caption{Patrones de E mal clasificados}
	\label{fig:PatronesMalE}
\end{figure}

La Figura \ref{fig:PatronesMalE} muestra algunos de los patrones de E que se han clasificado mal. El primer patrón ha sido clasificado como B, el segundo como F y el tercero como F.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{259_E.png}
	\includegraphics[scale=1]{260_A.png}
	\includegraphics[scale=1]{292_A.png}
	\caption{Patrones de F mal clasificados}
	\label{fig:PatronesMalF}
\end{figure}

La Figura \ref{fig:PatronesMalF} muestra algunos de los patrones de F que se han clasificado mal. El primer patrón ha sido clasificado como E, el segundo como A y el tercero como A.

\subsection{Tiempo de entrenamiento de noMNIST}
El entrenamiento de la red RBF con \emph{noMNIST}, utilizando la mejor configuración obtenida, ha tardado $8.22$ segundos. En el caso de la red MLP, el entrenamiento tardó aproximadamente 17 minutos. Se puede observar que el tiempo es muchísimo menor en la red RBF. Esto no se debe al modelo en sí, sino al algoritmo que se está utilizando para entrenarlo. En el caso del MLP, se utilizaba el algoritmo de retropropagación del error que tiene un coste computacional bastante alto. En el caso de RBF, se utiliza un algoritmo de clustering junto con una regresión logística, que tienen un coste computacional bastante menor.

\subsection{Mejor modelo para Iris}
En la Tabla \ref{table:CoeficientesIris}, se muestran los coeficientes para el mejor modelo de iris. La matriz de coeficientes se muestra traspuesta para una mejor visualización. De este modo, cada columna representa los coeficientes de cada clase.


\begin{table}[!htbp]
	\centering
	\begin{tabular}{r | r | r}
		$C_1$ & $C_2$ & $C_3$\\\hline
		$2.26462452$ & $-0.7614988$ & $-2.74575957$\\\hline
		$-0.95965088$ & $2.72752369$ & $-2.2884587$\\\hline
		$-1.44839092$ & $3.78681296$ & $-2.77558379$\\\hline
		$-1.32657568$ & $-0.07304431$ & $0.90464617$\\\hline
		$2.18647581$ & $-0.82364995$ & $-2.65084433$\\\hline
		$-1.42210155$ & $3.66934824$ & $-3.4604015$\\\hline
		$-1.40019232$ & $-2.32827791$ & $2.50586456$\\\hline
		$2.54346148$ & $-1.3183663$ & $-2.79783424$\\\hline
		$-3.33088353$ & $4.91765191$ & $-1.45777035$\\\hline
		$-1.43376605$ & $-5.87561702$ & $5.13752055$\\\hline
		$-1.15027771$ & $-2.02385112$ & $2.15844694$\\\hline
		$-1.03032993$ & $-2.07474903$ & $0.83738326$
	\end{tabular}
	\caption{Coeficientes del mejor modelo de \emph{iris}}
	\label{table:CoeficientesIris}
\end{table}

Estos coeficientes permiten calcular la probabilidad de pertenencia a cada una de la clases. Para ello, se utiliza la ecuación que se muestra a continuación:

\begin{equation}
P(x \in C_j) = o_j = \frac{exp(\beta_{jn} + \sum_{i=0}^{n-1} \beta_{ji} x_i)}{\sum_{l=1}^{k} exp(\beta_{ln} + \sum_{i=1}^{n-1} \beta_{li} x_i)}
\end{equation}

donde $n$ es el número de coeficientes (sin el sesgo), $k$ es el número de clases y $x_i$ es la salida de la neurona $i$.
En nuestro caso, el sesgo se ha añadido en la última columna de la matriz. Por ello, se utiliza $\beta_{xn}$ en lugar de $\beta_{x0}$. De igual forma, los sumatorios van desde 0 hasta $n-1$ en lugar de ir desde 1 hasta $n$.

Como ejemplo, a continuación, se muestra la ecuación del cálculo de la probabilidad de pertenencia a la clase 1. Por temas de espacio se han omitido parte de los sumandos pero se deben añadir todos ellos, utilizando los coeficientes que se muestran en la Tabla \ref{table:CoeficientesIris}.

\begin{equation}
P(x \in C_1) = o_j = \frac{exp(-1.0303 + 2.2646x_0 - 0.9596x_1 + ... - 1.1503x_{10})}
{P1 + P2 + P3}
\end{equation}

siendo:

\begin{equation}
P_1 = exp(-1.0303 + 2.2646x_0 + ... - 1.1503x_{10})
\end{equation}

\begin{equation}
P_2 = exp(-2.0747 - 0.7615x_0 + ... - 2.0239x_{10})
\end{equation}

\begin{equation}
P_3 = exp(0.8379 - 2.7458x_0 + ... + 2.1584x_{10})
\end{equation}

La probabilidad de pertenencia a las demás clases se calcularía de forma análoga.


\RaggedRight
\bibliographystyle{ieeetr}
\bibliography{memoria}

\end{document}