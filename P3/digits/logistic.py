# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 15:26:30 2017

@author: victor
"""

import pandas as pd

from sklearn.linear_model import LogisticRegression


digits_train = pd.read_csv('optdigits.tra')
digits_test = pd.read_csv('optdigits.tes')

digits_train_array = digits_train.values
digits_test_array = digits_test.values

X = digits_train_array[:,:-1]
Y = digits_train_array[:,-1]

logistic = LogisticRegression(solver='saga', multi_class='multinomial')
logistic.fit(X,Y)
print(logistic)

X_test = digits_test_array[:,:-1]
Y_test = digits_test_array[:,-1]

prediccion_test = logistic.predict(X_test)
print(prediccion_test)

precision = logistic.score(X_test, Y_test)
print(precision)
