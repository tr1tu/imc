import pandas as pd

from sklearn import neighbors

digits_train = pd.read_csv('optdigits.tra')
digits_test = pd.read_csv('optdigits.tes')

digits_train_array = digits_train.values
digits_test_array = digits_test.values

X = digits_train_array[:,:-1]
Y = digits_train_array[:,-1]

knn = neighbors.KNeighborsClassifier(n_neighbors=1)
knn.fit(X,Y)
print(knn)

X_test = digits_test_array[:,:-1]
Y_test = digits_test_array[:,-1]

prediccion_test = knn.predict(X_test)
print(prediccion_test)

precision = knn.score(X_test, Y_test)
print(precision)
