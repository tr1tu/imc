//============================================================================
// Introducción a los Modelos Computacionales
// Name        : practica1.cpp
// Author      : Pedro A. Gutiérrez
// Version     :
// Copyright   : Universidad de Córdoba
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <ctime>    // Para cojer la hora time()
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <string.h>
#include <math.h>
#include "imc/PerceptronMulticapa.h"

using namespace imc;
using namespace std;

struct CLIParams
{
  CLIParams ()
    : train(""),
	  test(""),
	  weights(""),
	  it(1000),
	  nOcultas(1),
	  nNeuronas(5),
	  eta(0.1),
	  mu(0.9),
	  rVal(0.0),
	  fDec(1),
	  online(false),
	  fError(0),
	  softmaxSalida(false)
    {}
  ~CLIParams(){}
  char train[250];
  char test[250];
  char weights[250];
  int it; // Iteraciones del bucle externo
  int nOcultas;
  int nNeuronas;
  double eta;
  double mu;
  double rVal; // Ratio de patrones para validacion
  double fDec; // Factor de decremento
  bool online;
  int fError; // Función de error
  bool softmaxSalida; // Usar función softmax en capa de salida


};

static void mostrarUso (const char * progname);
static int parseCLI (int argc, char* const* argv, CLIParams& params);

int main(int argc, char **argv) {
	// Procesar los argumentos de la línea de comandos

	CLIParams params;
	int numArgs;

	numArgs = parseCLI(argc, argv, params);

	if(numArgs < 2)
	{
		mostrarUso(argv[0]);
		return -1;
	}

	if(params.nOcultas < 1)
	{
		cerr << "Se debe usar al menos 1 capa oculta." << endl;
		exit(-1);
	}

	if(params.nNeuronas < 1)
	{
		cerr << "Se debe usar al menos 1 neurona por capa." << endl;
		exit(-1);
	}

	// Objeto perceptrón multicapa
	PerceptronMulticapa mlp;

	// Parámetros del mlp. Por ejemplo, mlp.dEta = valorQueSea;
	mlp.dEta = params.eta;
	mlp.dMu = params.mu;
	mlp.dValidacion = params.rVal;
	mlp.dDecremento = params.fDec;
	mlp.bOnline = params.online;
	int nCapasOcultas = params.nOcultas;
	int nNeuronas = params.nNeuronas;
	int iteraciones = params.it;
	int error = params.fError;

	// Lectura de datos de entrenamiento y test: llamar a mlp.leerDatos(...)
	Datos *pDatosTrain, *pDatosTest;
	pDatosTrain = mlp.leerDatos(params.train);
	if(pDatosTrain == NULL)
	{
		cerr << "El conjunto de datos de train no es válido. No se puede continuar." << endl;
		exit(-1);
	}
	pDatosTest = mlp.leerDatos(params.test);
	if(pDatosTest == NULL)
	{
		cerr << "No se especificó un conjunto de test válido. Usando conjunto de train para test." << endl;
		pDatosTest = mlp.leerDatos(params.train); // Si no hay test, usamos los de train.
	}

	// Inicializar vector topología
	int *topologia = new int[nCapasOcultas+2];
	topologia[0] = pDatosTrain->nNumEntradas;
	for(int i=1; i<(nCapasOcultas+2-1); i++)
		topologia[i] = nNeuronas;
	topologia[nCapasOcultas+2-1] = pDatosTrain->nNumSalidas;

	//Tipo de las neuronas
	int * tipo = new int[nCapasOcultas+2];
	for(int i = 0; i < nCapasOcultas + 2; i++)
		tipo[i] = 0;
	// Si queremos usar softmax en capa de salida
	if(params.softmaxSalida)
		tipo[nCapasOcultas+2-1] = 1;

	// Inicializar red con vector de topología
	mlp.inicializar(nCapasOcultas+2,topologia, tipo);


    // Semilla de los números aleatorios
    int semillas[] = {10,20,30,40,50};
    double *errores = new double[5];
    double *erroresTrain = new double[5];
    double *ccrs = new double[5];
    double *ccrsTrain = new double[5];
    int *numIter = new int[5];
    for(int i=0; i<5; i++){
    	cout << "**********" << endl;
    	cout << "SEMILLA " << semillas[i] << endl;
    	cout << "**********" << endl;
		srand(semillas[i]);
		mlp.ejecutarAlgoritmo(pDatosTrain,pDatosTest,iteraciones,&(erroresTrain[i]),&(errores[i]),&(ccrsTrain[i]),&(ccrs[i]),error, numIter[i]);
		cout << "Finalizamos => CCR de test final: " << ccrs[i] << endl;
    }


    double mediaError = 0, desviacionTipicaError = 0;
    double mediaErrorTrain = 0, desviacionTipicaErrorTrain = 0;
    double mediaCCR = 0, desviacionTipicaCCR = 0;
    double mediaCCRTrain = 0, desviacionTipicaCCRTrain = 0;
    double mediaIter = 0;

    // Calcular medias y desviaciones típicas de entrenamiento y test

	for (int i = 0; i < 5; i++)
	{
		mediaError += errores[i];
		mediaErrorTrain += erroresTrain[i];
		mediaCCR += ccrs[i];
		mediaCCRTrain += ccrsTrain[i];
		mediaIter += numIter[i];
	}

	mediaError /= 5;
	mediaErrorTrain /= 5;
	mediaCCR /= 5;
	mediaCCRTrain /= 5;
	mediaIter /= 5.;

	for (int i = 0; i < 5; i++)
	{
		desviacionTipicaError += pow(errores[i] - mediaError, 2);
		desviacionTipicaErrorTrain += pow(erroresTrain[i] - mediaErrorTrain, 2);
		desviacionTipicaCCR += pow(ccrs[i] - mediaCCR, 2);
		desviacionTipicaCCRTrain += pow(ccrsTrain[i] - mediaCCRTrain, 2);
	}

	desviacionTipicaError = sqrt(desviacionTipicaError/5);
	desviacionTipicaErrorTrain = sqrt(desviacionTipicaErrorTrain/5);
	desviacionTipicaCCR = sqrt(desviacionTipicaCCR/5);
	desviacionTipicaCCRTrain = sqrt(desviacionTipicaCCRTrain/5);


    cout << "HEMOS TERMINADO TODAS LAS SEMILLAS" << endl;

	cout << "INFORME FINAL" << endl;
	cout << "*************" << endl;
    cout << "Error de entrenamiento (Media +- DT): " << mediaErrorTrain << " +- " << desviacionTipicaErrorTrain << endl;
    cout << "Error de test (Media +- DT): " << mediaError << " +- " << desviacionTipicaError << endl;
    cout << "CCR de entrenamiento (Media +- DT): " << mediaCCRTrain << " +- " << desviacionTipicaCCRTrain << endl;
    cout << "CCR de test (Media +- DT): " << mediaCCR << " +- " << desviacionTipicaCCR << endl;
    cout << "Número medio de iteraciones: " << mediaIter << endl;


    // Liberar memoria
    delete[] topologia;
    delete[] tipo;
    delete[] errores;
    delete[] erroresTrain;
    delete[] ccrs;
    delete[] ccrsTrain;
    delete[] numIter;

	return EXIT_SUCCESS;
}

static void mostrarUso (const char * progname)
{
  cout << "Modelo de perceptrón multicapa" << std::endl;
  cout << "Uso: " << progname << " -t <train_dataset> [-T <test_dataset> | -w <weights_output> | -i <iterations> | -l <hidden layers> | -e <eta value> |" <<
		  "-m <mu value> | -d <decrement factor> | -v <% validation>] | -o | -f <error function> | -s ]" << std::endl;
}

static int parseCLI (int argc, char* const* argv, CLIParams& params)
{
	int opcion;
	while ((opcion = getopt (argc, argv, "t:T:w:i:l:h:e:m:v:d:of:s")) != -1)
	{
		switch (opcion)
		{

		case 't':
			strcpy(params.train, optarg);
			break;

		case 'T':
			strcpy(params.test, optarg);
			break;

		case 'w':
			strcpy(params.weights, optarg);
			break;

		case 'i':
			params.it = atoi(optarg);
			break;

		case 'l':
			params.nOcultas = atoi(optarg);
			break;

		case 'h':
			params.nNeuronas = atoi(optarg);
			break;

		case 'e':
			params.eta = atof(optarg);
			break;

		case 'm':
			params.mu = atof(optarg);
			break;

		case 'v':
			params.rVal = atof(optarg);
			break;

		case 'd':
			params.fDec = atof(optarg);
			break;

		case 'o':
			params.online = true;
			break;

		case 'f':
			params.fError = atoi(optarg);
			break;

		case 's':
			params.softmaxSalida = true;
			break;

		case '?': // en caso de error getopt devuelve el caracter ?

			if (isprint (optopt))
				cerr << "Error: Opción desconocida \'" << optopt
				<< "\'" << std::endl;
			else
				cerr << "Error: Caracter de opcion desconocido \'x" << hex << optopt
				<< "\'" << std::endl;
			mostrarUso(argv[0]);
			exit (EXIT_FAILURE);

			// en cualquier otro caso lo consideramos error grave y salimos
		default:
			cerr << "Error: línea de comandos errónea." << endl;
			mostrarUso(argv[0]);
			exit(EXIT_FAILURE);
		}  // case

	}// while
	return optind;
}


