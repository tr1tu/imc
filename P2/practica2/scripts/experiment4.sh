#!/bin/bash

if [ ! -d results4 ];
then
	mkdir results4
fi

if [ ! $# -eq 2 ];
	then
	echo "Parámetros incorrectos: "$0" <ruta del ejecutable> <ruta de las bases de datos>"
	exit
fi

for v in 0; do
	for d in 1 2; do
		$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v $v -d $d -f 1 -s -o > "results4/xor_"$v"_"$d".txt" &
	done
done


for v in 0 0.1 0.2; do
	for d in 1 2; do
		$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v $v -d $d -f 1 -s > "results4/iris_"$v"_"$d".txt" &
	done
done

for v in 0 0.1 0.2; do
	for d in 1 2; do
		$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v $v -d $d -f 1 -s > "results4/nomnist_"$v"_"$d".txt" &
	done
done