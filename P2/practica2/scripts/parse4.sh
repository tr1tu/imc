#!/bin/bash

cd results4

if [ $# -eq 1 ] && [ $1 == "latex" ]; then

	s=""

	for f in $(find .); do
		if [ -f $f ]; then
			bd=$(echo $f | sed -r -n -e "s/\.\/([a-Z]+)_[0-9\.]+_[0-9\.]+\.txt/\1/gp")
			l=$(echo $f | sed -r -n -e "s/\.\/[a-Z]+_([0-9\.]+)_[0-9\.]+\.txt/\1/gp")
			h=$(echo $f | sed -r -n -e "s/\.\/[a-Z]+_[0-9\.]+_([0-9\.]+)\.txt/\1/gp")
			#s=$s$bd" & "$l" & "$h" & "$(cat $f | sed -r -n -e "s/Error de entrenamiento \(Media \+- DT\): +([0-9]\.[0-9]+e?[-+]?[0-9]+) \+- ([0-9]\.[0-9]+e?[-+]?[0-9]+)/\$\1\$ \& \$\2\$/gp")' & '$(cat $f | sed -r -n -e "s/Error de test \(Media \+- DT\): +([0-9]\.[0-9]+e?[-+]?[0-9]+) \+- ([0-9]\.[0-9]+e?[-+]?[0-9]+)/\$\1\$ \& \$\2\$/gp")" & "$(cat $f | sed -r -n -e "s/CCR de entrenamiento \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\1 \2/gp")" & "$(cat $f | sed -r -n -e "s/CCR de test \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\1 \2/gp")" & "$(cat $f | sed -r -n -e "s/Número medio de iteraciones: +([0-9\.]+)/\1/gp")'\\\\\n'
			s=$s$bd" & "$l" & "$h" & "$(cat $f | sed -r -n -e "s/CCR de entrenamiento \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\$\1\$ \& \$\2\$/gp")" & "$(cat $f | sed -r -n -e "s/CCR de test \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\$\1\$ \& \$\2\$/gp")" & "$(cat $f | sed -r -n -e "s/Número medio de iteraciones: +([0-9\.]+)/\$\1\$/gp")'\\\\\n'
		fi
	done

	echo 'Dataset & \emph{v} & \emph{d} & $\overline{CCR}$ Train & $\sigma_{CCR}$ Train & $\overline{CCR}$ Test & $\sigma_{CCR}$ Test\\\hline'
	echo -e $s | sort -k1,1 -k3n,3 -k5n,5	
else
	
	s=""

	for f in $(find .); do
		if [ -f $f ]; then
			bd=$(echo $f | sed -r -n -e "s/\.\/([a-Z]+)_[0-9\.]+_[0-9\.]+\.txt/\1/gp")
			l=$(echo $f | sed -r -n -e "s/\.\/[a-Z]+_([0-9\.]+)_[0-9\.]+\.txt/\1/gp")
			h=$(echo $f | sed -r -n -e "s/\.\/[a-Z]+_[0-9\.]+_([0-9\.]+)\.txt/\1/gp")
			s=$s$bd" "$l" "$h" "$(cat $f | sed -r -n -e "s/Error de entrenamiento \(Media \+- DT\): +([0-9]\.[0-9]+e?[-+]?[0-9]+) \+- ([0-9]\.[0-9]+e?[-+]?[0-9]+)/\1 \2/gp")" "$(cat $f | sed -r -n -e "s/Error de test \(Media \+- DT\): +([0-9]\.[0-9]+e?[-+]?[0-9]+) \+- ([0-9]\.[0-9]+e?[-+]?[0-9]+)/\1 \2/gp")" "$(cat $f | sed -r -n -e "s/CCR de entrenamiento \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\1 \2/gp")" "$(cat $f | sed -r -n -e "s/CCR de test \(Media \+- DT\): ([0-9\.e\+-]+) \+- ([0-9\.e\+-]+)/\1 \2/gp")" "$(cat $f | sed -r -n -e "s/Número medio de iteraciones: +([0-9\.]+)/\1/gp")"\n" 
		fi
	done

	echo -e $s | sort -k1,1 -k2n,2 -k3n,3	
fi

