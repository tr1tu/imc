#!/bin/bash

if [ ! -d results2 ];
then
	mkdir results2
fi

if [ ! $# -eq 2 ];
	then
	echo "Parámetros incorrectos: "$0" <ruta del ejecutable> <ruta de las bases de datos>"
	exit
fi

#MSE y sigmoide
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v 0 -d 1 -f 0 > "results2/xor_0_0.txt" &
$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v 0.1 -d 1 -f 0 > "results2/iris_0_0.txt" &
$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v 0.1 -d 1 -f 0 > "results2/nomnist_0_0.txt" &

#MSE y softmax
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v 0 -d 1 -f 0 -s > "results2/xor_0_1.txt" &
$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v 0.1 -d 1 -f 0 -s > "results2/iris_0_1.txt" &
$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v 0.1 -d 1 -f 0 -s > "results2/nomnist_0_1.txt" &

#Entropía cruzada y softmax
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v 0 -d 1 -f 1 -s > "results2/xor_1_1.txt" &
$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s > "results2/iris_1_1.txt" &
$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s > "results2/nomnist_1_1.txt" &