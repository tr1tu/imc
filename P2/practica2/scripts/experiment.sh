#!/bin/bash

if [ ! -d results ];
then
	mkdir results
fi

if [ ! $# -eq 2 ];
	then
	echo "Parámetros incorrectos: "$0" <ruta del ejecutable> <ruta de las bases de datos>"
	exit
fi

pids=""

# Mi mejor arquitectura en la p1 para xor fue 2 capas ocultas de 100 neuronas
l=2
h=100
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l $l -h $h -e 0.7 -m 1 -v 0.0 -d 1 -f 1 -s > "results/xor_"$l"_"$h".txt" &
echo "Lanzado xor con $l capas con $h neuronas ($!)"
pids="$pids $!"

for bd in iris nomnist; do
	for l in 1 2; do
		for h in 5 10 50 75; do
			$1 -t $2/train_$bd.dat -T $2/test_$bd.dat -i 1000 -l $l -h $h -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s > "results/"$bd"_"$l"_"$h".txt" &
			echo "Lanzado $bd con $l capas con $h neuronas ($!)"
			pids="$pids $!"
		done 
	done
done

for pid in $pids; do
    wait $pid
    echo "Terminado $pid"
done