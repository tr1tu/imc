#!/bin/bash

if [ ! -d results3 ];
then
	mkdir results3
fi

if [ ! $# -eq 2 ];
	then
	echo "Parámetros incorrectos: "$0" <ruta del ejecutable> <ruta de las bases de datos>"
	exit
fi

# Offline
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v 0 -d 1 -f 1 -s > "results3/xor_0.txt" &
$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s > "results3/iris_0.txt" &
$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s > "results3/nomnist_0.txt" &

# Online
$1 -t $2/train_xor.dat -T $2/test_xor.dat -i 1000 -l 2 -h 100 -e 0.7 -m 1 -v 0 -d 1 -f 1 -s -o > "results3/xor_1.txt" &
$1 -t $2/train_iris.dat -T $2/test_iris.dat -i 1000 -l 2 -h 75 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s -o > "results3/iris_1.txt" &
$1 -t $2/train_nomnist.dat -T $2/test_nomnist.dat -i 1000 -l 2 -h 50 -e 0.7 -m 1 -v 0.1 -d 1 -f 1 -s -o > "results3/nomnist_1.txt" &