/*********************************************************************
 * File  : PerceptronMulticapa.cpp
 * Date  : 2017
 *********************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>

#include "PerceptronMulticapa.h"
#include "util.h"

using namespace imc;
using namespace std;
using namespace util;


// ------------------------------
// CONSTRUCTOR: Dar valor por defecto a todos los parámetros (dEta, dMu, dValidacion y dDecremento)
PerceptronMulticapa::PerceptronMulticapa(){
	nNumCapas = 3; // 1 oculta + 1 salida
	pCapas = NULL; //Dejamos la reserva para la funcion inicializar.
	dEta = 0.1;
	dMu = 0.9;
	dValidacion = 0.0;
	dDecremento = 1;
	bOnline = 1;
	nNumPatronesTrain = 0;
}

// ------------------------------
// Reservar memoria para las estructuras de datos
// nl tiene el numero de capas y npl es un vector que contiene el número de neuronas por cada una de las capas
// tipo contiene el tipo de cada capa (0 => sigmoide, 1 => softmax)
// Rellenar vector Capa* pCapas
int PerceptronMulticapa::inicializar(int nl, int npl[], int tipo[]) {
	pCapas = new Capa[nl];

	// Capa de entrada
	pCapas[0].nNumNeuronas = npl[0];
	pCapas[0].pNeuronas = new Neurona[npl[0]];
	pCapas[0].tipo = tipo[0];

	//Capas ocultas y capa de salida
	for(int i = 1; i < nl; i++)
	{
		pCapas[i].nNumNeuronas = npl[i];
		pCapas[i].pNeuronas = new Neurona[npl[i]];
		pCapas[i].tipo = tipo[i];

		for(int j = 0; j < npl[i]; j++)
		{
			pCapas[i].pNeuronas[j].w = new double[pCapas[i-1].nNumNeuronas + 1]; // + 1 para sesgo
			pCapas[i].pNeuronas[j].deltaW = new double[pCapas[i-1].nNumNeuronas + 1];
			pCapas[i].pNeuronas[j].ultimoDeltaW = new double[pCapas[i-1].nNumNeuronas + 1];
			pCapas[i].pNeuronas[j].wCopia = new double[pCapas[i-1].nNumNeuronas + 1];
		}
	}

	nNumCapas = nl;

	return 1;
}


// ------------------------------
// DESTRUCTOR: liberar memoria
PerceptronMulticapa::~PerceptronMulticapa() {
	liberarMemoria();
}


// ------------------------------
// Liberar memoria para las estructuras de datos
void PerceptronMulticapa::liberarMemoria() {
	for(int i = 1; i < nNumCapas; i++)
	{
		// La capa de entrada no tiene pesos.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			delete[] pCapas[i].pNeuronas[j].w;
			delete[] pCapas[i].pNeuronas[j].deltaW;
			delete[] pCapas[i].pNeuronas[j].ultimoDeltaW;
			delete[] pCapas[i].pNeuronas[j].wCopia;
		}

		delete[] pCapas[i].pNeuronas;
	}

	delete[] pCapas;
}

// ------------------------------
// Rellenar todos los pesos (w) aleatoriamente entre -1 y 1
void PerceptronMulticapa::pesosAleatorios() {
	// La capa de entrada no tiene pesos.
	for(int i = 1; i < nNumCapas; i++)
	{
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++) //<= por el sesgo.
			{
				pCapas[i].pNeuronas[j].w[k] = ((double) rand() / (RAND_MAX)) * 2 - 1;
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
				pCapas[i].pNeuronas[j].deltaW[k] = 0;
				pCapas[i].pNeuronas[j].ultimoDeltaW[k] = 0;
			}
		}
	}
}

// ------------------------------
// Alimentar las neuronas de entrada de la red con un patrón pasado como argumento
void PerceptronMulticapa::alimentarEntradas(double* input) {
	for(int i = 0; i < pCapas[0].nNumNeuronas; i++)
	{
		pCapas[0].pNeuronas[i].x = input[i];
	}
}

// ------------------------------
// Recoger los valores predichos por la red (out de la capa de salida) y almacenarlos en el vector pasado como argumento
void PerceptronMulticapa::recogerSalidas(double* output){
	//Reservamos el vector.

	for(int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++)
	{
		output[i] = pCapas[nNumCapas-1].pNeuronas[i].x;
	}
}

// ------------------------------
// Hacer una copia de todos los pesos (copiar w en copiaW)
void PerceptronMulticapa::copiarPesos() {
	for(int i = 1; i < nNumCapas; i++)
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
				pCapas[i].pNeuronas[j].wCopia[k] = pCapas[i].pNeuronas[j].w[k];
}

// ------------------------------
// Restaurar una copia de todos los pesos (copiar copiaW en w)
void PerceptronMulticapa::restaurarPesos() {
	for(int i = 1; i < nNumCapas; i++)
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
				for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
					pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].wCopia[k];
}

// ------------------------------
// Calcular y propagar las salidas de las neuronas, desde la primera capa hasta la última
void PerceptronMulticapa::propagarEntradas() {
	for(int i = 1; i < nNumCapas; i++)
	{
		// Vector de nets de todas las neuronas de la capa
		double * nets = new double[pCapas[i].nNumNeuronas];
		// Sumatorio de los nets de todas las neuronas de la capa
		double netSum = .0;

		// Primero se rellenará el vector nets y se calculará netSum
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			nets[j] = .0;

			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				nets[j] += pCapas[i-1].pNeuronas[k].x * pCapas[i].pNeuronas[j].w[k];
			}

			nets[j] += pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas]; // Sesgo
			netSum += exp(nets[j]);


		}

		// Después se calcula el valor de salida de cada neurona
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			if(pCapas[i].tipo == 0) // Sigmoide
				pCapas[i].pNeuronas[j].x = (1.0/(1+exp(-nets[j])));
			else // Softmax
				pCapas[i].pNeuronas[j].x = exp(nets[j])/netSum;

		}

		delete[] nets;
	}

}

// ------------------------------
// Calcular el error de salida del out de la capa de salida con respecto a un vector objetivo y devolverlo
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::calcularErrorSalida(double* target, int funcionError) {

	double error = 0;

	if(funcionError == 1) // Entropia cruzada
	{
		for(int i = 0; i < pCapas[nNumCapas - 1].nNumNeuronas; i++)
		{
			error += - target[i] * log(pCapas[nNumCapas - 1].pNeuronas[i].x);
		}

		error = error / pCapas[nNumCapas - 1].nNumNeuronas;

	}
	else // MSE
	{
		for(int i = 0; i < pCapas[nNumCapas - 1].nNumNeuronas; i++)
		{
			error += pow(target[i] - pCapas[nNumCapas - 1].pNeuronas[i].x, 2);
		}

		error = error / pCapas[nNumCapas - 1].nNumNeuronas;
	}

	return error;
}


// ------------------------------
// Retropropagar el error de salida con respecto a un vector pasado como argumento, desde la última capa hasta la primera
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::retropropagarError(double* objetivo, int funcionError) {
	// Neuronas de la capa de salida
	if(pCapas[nNumCapas - 1].tipo == 0) // Sigmoide
	{
		for(int j = 0; j < pCapas[nNumCapas - 1].nNumNeuronas; j++)
		{
			double outj = pCapas[nNumCapas - 1].pNeuronas[j].x;

			if(funcionError == 0) // MSE
				pCapas[nNumCapas-1].pNeuronas[j].dX = -(objetivo[j] - outj) * outj * (1 - outj);
			else // Entropía cruzada
				pCapas[nNumCapas-1].pNeuronas[j].dX = -(objetivo[j] / outj) * outj * (1 - outj);
		}

	}
	else // Softmax
	{
		for(int j = 0; j < pCapas[nNumCapas-1].nNumNeuronas; j++)
		{
			double outj = pCapas[nNumCapas-1].pNeuronas[j].x;
			double sum = 0.0;

			for(int i = 0; i < pCapas[nNumCapas-1].nNumNeuronas; i++)
			{
				double outi = pCapas[nNumCapas-1].pNeuronas[i].x;
				if(funcionError == 0) // MSE
					sum += (objetivo[i] - outi) * outj * ((i == j ? 1 : 0) - outi);
				else // Entropía cruzada
					sum += (objetivo[i] / outi) * outj * ((i == j ? 1 : 0) - outi);
			}
			pCapas[nNumCapas-1].pNeuronas[j].dX = -sum;
		}
	}

	// Neuronas de las capas ocultas
	for (int h = nNumCapas - 2; h >= 1; h--) // h >= 1, en la capa de entrada no hay derivada.
	{
		for (int j = 0; j < pCapas[h].nNumNeuronas; j++)
		{
			//Capas ocultas -> SIGMOIDE SIEMPRE
			double s = 0.0;
			double out = pCapas[h].pNeuronas[j].x;
			for(int i = 0; i < pCapas[h+1].nNumNeuronas; i++)
			{
				s += pCapas[h+1].pNeuronas[i].w[j] * pCapas[h+1].pNeuronas[i].dX;
			}
			pCapas[h].pNeuronas[j].dX = s * out * (1 - out);

		}
	}



}

// ------------------------------
// Acumular los cambios producidos por un patrón en deltaW
void PerceptronMulticapa::acumularCambio() {

	// Para cada capa (menos la de entrada)
	for(int i = 1; i < nNumCapas; i++)
	{
		// Para cada neurona de la capa i.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			// Para cada neurona de la capa i - 1
			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				// Calcular el nuevo
				pCapas[i].pNeuronas[j].deltaW[k] += pCapas[i].pNeuronas[j].dX * pCapas[i-1].pNeuronas[k].x;
			}

			// Actualizar el deltaW del sesgo
			pCapas[i].pNeuronas[j].deltaW[pCapas[i-1].nNumNeuronas] += pCapas[i].pNeuronas[j].dX * 1;
		}
	}
}

// ------------------------------
// Actualizar los pesos de la red, desde la primera capa hasta la última
void PerceptronMulticapa::ajustarPesos() {
	for(int i = 1; i < nNumCapas; i++)
	{
		//Calculamos la eta para esta capa usando el factor de decremento.
		double etaI = pow(dDecremento, -(nNumCapas-i)) * dEta;

		// Para cada neurona de la capa i.
		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			// Para cada neurona de la capa i - 1
			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
			{
				// Actualizar pesos
				pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].w[k] - etaI * pCapas[i].pNeuronas[j].deltaW[k]
											  - dMu * etaI * pCapas[i].pNeuronas[j].ultimoDeltaW[k];
			}
			// Actualizar sesgo
			pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] = pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] -
					etaI * pCapas[i].pNeuronas[j].deltaW[pCapas[i-1].nNumNeuronas] - etaI * dEta * pCapas[i].pNeuronas[j].ultimoDeltaW[pCapas[i-1].nNumNeuronas];
		}
	}
}

// ------------------------------
// Imprimir la red, es decir, todas las matrices de pesos
void PerceptronMulticapa::imprimirRed() {
	// Para cada capa menos la de entrada
	for(int i = 1; i < nNumCapas; i++)
	{
		cout << "Capa " << i << endl;
		cout << "------------------" << endl;

		for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
		{
			cout << pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] << " ";

			for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
				cout << pCapas[i].pNeuronas[j].w[k] << " ";

			cout << endl;
		}
	}
}

// ------------------------------
// Simular la red: propragar las entradas hacia delante, computar el error, retropropagar el error y ajustar los pesos
// entrada es el vector de entradas del patrón, objetivo es el vector de salidas deseadas del patrón.
// El paso de ajustar pesos solo deberá hacerse si el algoritmo es on-line
// Si no lo es, el ajuste de pesos hay que hacerlo en la función "entrenar"
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::simularRed(double* entrada, double* objetivo, int funcionError) {

	if(bOnline) // Solo inicializar los deltas si es online, en caso contrario, se hace en "entrenar"
		for(int i = 1; i < nNumCapas; i++)
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
				for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
				{
					// Guardar el ultimo deltaW
					pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
					//Inicializar deltaW
					pCapas[i].pNeuronas[j].deltaW[k] = 0;
				}

	alimentarEntradas(entrada);
	propagarEntradas();
	retropropagarError(objetivo, funcionError);
	acumularCambio();

	if(bOnline) //Solo ajustar si es online
	{
		ajustarPesos();
	}
}

// ------------------------------
// Leer una matriz de datos a partir de un nombre de fichero y devolverla
Datos* PerceptronMulticapa::leerDatos(const char *archivo) {

	// Objeto de lectura de fichero
	ifstream f(archivo);

	if(!f.is_open())
		return NULL;

	// Estructura que almacenará los datos
	Datos *datos = new Datos;

	// Lectura de las dimensiones
	f >> datos->nNumEntradas >> datos->nNumSalidas >> datos->nNumPatrones;

	// Reserva de memoria para las entradas y salidas.
	datos->entradas = new double*[datos->nNumPatrones];
	for(int i = 0; i < datos->nNumPatrones; i++)
		datos->entradas[i] = new double[datos->nNumEntradas];
	datos->salidas = new double*[datos->nNumPatrones];
		for(int i = 0; i < datos->nNumPatrones; i++)
			datos->salidas[i] = new double[datos->nNumSalidas];

	// Lectura de los datos
	for (int i = 0; i < datos->nNumPatrones; i++)
	{
		// Leer las entradas
		for (int j = 0; j < datos->nNumEntradas; j++)
			f >> datos->entradas[i][j];

		// Leer las salidas
		for (int j = 0; j < datos->nNumSalidas; j++)
			f >> datos->salidas[i][j];
	}

	// Cerrar el fichero
	f.close();

	// La estructura datos se debe liberar cuando no se vaya a utilizar mas.
	return datos;
}


// ------------------------------
// Entrenar la red para un determinado fichero de datos (pasar una vez por todos los patrones)
void PerceptronMulticapa::entrenar(Datos* pDatosTrain, int funcionError) {

	if(!bOnline)
		for(int i = 1; i < nNumCapas; i++)
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
				for(int k = 0; k <= pCapas[i-1].nNumNeuronas; k++)
				{
					// Guardar el ultimo deltaW
					pCapas[i].pNeuronas[j].ultimoDeltaW[k] = pCapas[i].pNeuronas[j].deltaW[k];
					//Inicializar deltaW
					pCapas[i].pNeuronas[j].deltaW[k] = 0;
				}


	for(int i=0; i<pDatosTrain->nNumPatrones; i++){
		simularRed(pDatosTrain->entradas[i], pDatosTrain->salidas[i], funcionError);
	}

	// Actualizar pesos si es offline
	if(!bOnline)
		for(int i = 1; i < nNumCapas; i++)
		{
			//Calculamos la eta para esta capa usando el factor de decremento.
			double etaI = pow(dDecremento, -(nNumCapas-i)) * dEta;

			// Para cada neurona de la capa i.
			for(int j = 0; j < pCapas[i].nNumNeuronas; j++)
			{
				// Para cada neurona de la capa i - 1
				for(int k = 0; k < pCapas[i-1].nNumNeuronas; k++)
				{
					// Actualizar pesos
					pCapas[i].pNeuronas[j].w[k] = pCapas[i].pNeuronas[j].w[k] - etaI * pCapas[i].pNeuronas[j].deltaW[k] / nNumPatronesTrain
							 - dMu * etaI * pCapas[i].pNeuronas[j].ultimoDeltaW[k] / nNumPatronesTrain;


				}

				// Actualizar sesgo
				pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] = pCapas[i].pNeuronas[j].w[pCapas[i-1].nNumNeuronas] -
															   etaI * pCapas[i].pNeuronas[j].deltaW[pCapas[i-1].nNumNeuronas] / nNumPatronesTrain
															 - etaI * dEta * pCapas[i].pNeuronas[j].ultimoDeltaW[pCapas[i-1].nNumNeuronas] / nNumPatronesTrain;

			}
		}
}

// ------------------------------
// Probar la red con un conjunto de datos y devolver el error cometido
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
double PerceptronMulticapa::test(Datos* pDatosTest, int funcionError) {
	double errorMedio = 0;

	for (int i = 0; i < pDatosTest->nNumPatrones; i++)
	{
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		double error = calcularErrorSalida(pDatosTest->salidas[i], funcionError);
		errorMedio += error;

	}

	errorMedio /= pDatosTest->nNumPatrones;

	return errorMedio;
}


// ------------------------------
// Probar la red con un conjunto de datos y devolver el CCR
double PerceptronMulticapa::testClassification(Datos* pDatosTest) {
	double ccr = 0;
	double * salidasObtenidas = new double[pCapas[nNumCapas-1].nNumNeuronas];


	for(int i = 0; i < pDatosTest->nNumPatrones; i++)
	{
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();

		for(int j = 0; j < pCapas[nNumCapas-1].nNumNeuronas; j++)
			salidasObtenidas[j] = pCapas[nNumCapas-1].pNeuronas[j].x;

		if(argMax(pDatosTest->salidas[i], pDatosTest->nNumSalidas) == argMax(salidasObtenidas, pCapas[nNumCapas-1].nNumNeuronas))
		{
			ccr += 1.0;

		}

	}

	delete[] salidasObtenidas;
	ccr = 100.0 * ccr / pDatosTest->nNumPatrones;


	return ccr;
}

// ------------------------------
// Ejecutar el algoritmo de entrenamiento durante un número de iteraciones, utilizando pDatosTrain
// Una vez terminado, probar como funciona la red en pDatosTest
// Tanto el error MSE de entrenamiento como el error MSE de test debe calcularse y almacenarse en errorTrain y errorTest
// funcionError=1 => EntropiaCruzada // funcionError=0 => MSE
void PerceptronMulticapa::ejecutarAlgoritmo(Datos * pDatosTrain, Datos * pDatosTest, int maxiter, double *errorTrain, double *errorTest, double *ccrTrain, double *ccrTest, int funcionError, int &numIter)
{
	int countTrain = 0;

	// Inicialización de pesos
	pesosAleatorios();

	double minTrainError = 0;
	double minValidationError = 0;
	int numSinMejorar = 0;
	int numSinMejoraVal = 0;
	double testError = 0;
	//nNumPatronesTrain = pDatosTrain->nNumPatrones;

	double validationError = 0.0;

	Datos * pNuevosDatosTrain = new Datos;
	Datos * pDatosValidacion = new Datos;

	// Generar datos de validación
	if(dValidacion > 0 && dValidacion < 1){
		// Divide el conjunto de train en train y validacion.
		dividirTrain(pDatosTrain, pNuevosDatosTrain, pDatosValidacion, dValidacion);
		// Modificación que solo afecta dentro de esta función
		pDatosTrain = pNuevosDatosTrain;
	}

	nNumPatronesTrain = pDatosTrain->nNumPatrones;

	numIter = -1;

	// Aprendizaje del algoritmo
	do {

		entrenar(pDatosTrain,funcionError);
		double trainError = test(pDatosTrain,funcionError);
		if(countTrain==0 || fabs(trainError - minTrainError) > 0.00001){
			minTrainError = trainError;
			copiarPesos();
			numSinMejorar = 0;
		}
		else{
			numSinMejorar++;
		}

		if(numSinMejorar==50)
		{
			numIter = countTrain;
			countTrain = maxiter;
		}
		testError = test(pDatosTest,funcionError);

		// Comprobar condiciones de parada de validación y forzar
		if(dValidacion > 0 && dValidacion < 1)
		{
			validationError = test(pDatosValidacion, funcionError);
			if (countTrain == 0 || fabs(validationError - minValidationError) > 0.00001)
			{
				minValidationError = validationError;
				copiarPesos();
				numSinMejoraVal = 0;
			}
			else
				numSinMejoraVal++;

			if(numSinMejoraVal == 50)
			{
				restaurarPesos();
				numIter = countTrain;
				countTrain = maxiter;
			}
		}

		countTrain++;


		cout << "Iteración " << countTrain << "\t Error de entrenamiento: " << trainError << "\t Error de test: " << testError << "\t Error de validacion: " << validationError << endl;
		//cout << countTrain << "," << testClassification(pDatosTrain) << "," << testClassification(pDatosValidacion) << "," << testClassification(pDatosTest) << endl;

	} while ( countTrain<maxiter );

	if(numIter == -1)
		numIter = countTrain;

	restaurarPesos();

	cout << "PESOS DE LA RED" << endl;
	cout << "===============" << endl;
	imprimirRed();

	double ** predicciones = new double*[pDatosTest->nNumPatrones];

	cout << "Salida Esperada Vs Salida Obtenida (test)" << endl;
	cout << "=========================================" << endl;
	for(int i=0; i<pDatosTest->nNumPatrones; i++){
		double* prediccion = new double[pDatosTest->nNumSalidas];
		predicciones[i] = prediccion;

		// Cargamos las entradas y propagamos el valor
		alimentarEntradas(pDatosTest->entradas[i]);
		propagarEntradas();
		recogerSalidas(prediccion);
		//cout << argMax(pDatosTest->salidas[i], pDatosTest->nNumSalidas) << " - " << argMax(prediccion, pDatosTest->nNumSalidas) << endl;
		for(int j=0; j<pDatosTest->nNumSalidas; j++)
			cout << pDatosTest->salidas[i][j] << " -- " << prediccion[j] << " \\\\ " ;
		cout << endl;

	}

	cout << endl << "Matriz de confusión" << endl << "=====================" << endl;
	imprimirMatrizConfusion(pDatosTest, predicciones);
	cout << endl;

	for(int i = 0; i < pDatosTest->nNumPatrones; i++)
		delete[] predicciones[i];

	delete[] predicciones;

	*errorTest=test(pDatosTest,funcionError);
	*errorTrain=minTrainError;
	*ccrTest = testClassification(pDatosTest);
	*ccrTrain = testClassification(pDatosTrain);

}

void PerceptronMulticapa::dividirTrain(Datos * pDatosTrain, Datos * pNuevosDatosTrain, Datos * pDatosValidacion, double v)
{
	// Dimensiones de validacion
	pDatosValidacion->nNumPatrones = round(pDatosTrain->nNumPatrones * dValidacion);
	pDatosValidacion->nNumEntradas = pDatosTrain->nNumEntradas;
	pDatosValidacion->nNumSalidas = pDatosTrain->nNumSalidas;

	//Dimensiones del nuevo train
	pNuevosDatosTrain->nNumPatrones = pDatosTrain->nNumPatrones - pDatosValidacion->nNumPatrones;
	pNuevosDatosTrain->nNumEntradas = pDatosTrain->nNumEntradas;
	pNuevosDatosTrain->nNumSalidas = pDatosTrain->nNumSalidas;

	// Vector que contiene los indices de los patrones en un orden aleatorio.
	int * indicesTrain = vectorAleatoriosEnterosSinRepeticion(0, pDatosTrain->nNumPatrones-1, pDatosTrain->nNumPatrones);
	int * indicesValidacion = indicesTrain + pNuevosDatosTrain->nNumPatrones;

	// Nuevos vectores de train (reserva)
	pNuevosDatosTrain->entradas = new double*[pNuevosDatosTrain->nNumPatrones];
	pNuevosDatosTrain->salidas = new double*[pNuevosDatosTrain->nNumPatrones];
	for(int i = 0; i < pNuevosDatosTrain->nNumPatrones; i++)
	{
		pNuevosDatosTrain->entradas[i] = new double[pNuevosDatosTrain->nNumEntradas];
		pNuevosDatosTrain->salidas[i] = new double[pNuevosDatosTrain->nNumSalidas];
	}

	// Vectores de validacion (reserva)
	pDatosValidacion->entradas = new double*[pDatosValidacion->nNumPatrones];
	pDatosValidacion->salidas = new double*[pDatosValidacion->nNumPatrones];
	for(int i = 0; i < pDatosValidacion->nNumPatrones; i++)
	{
		pDatosValidacion->entradas[i] = new double[pDatosValidacion->nNumEntradas];
		pDatosValidacion->salidas[i] = new double[pDatosValidacion->nNumSalidas];
	}


	// Rellenar ambos vectores
	// Los indices de los patrones de train estan en indicesTrain (pNuevosDatosTrain->nNumPatrones)
	for(int i = 0; i < pNuevosDatosTrain->nNumPatrones; i++) // Train
	{
		int idx = indicesTrain[i];

		for(int j = 0; j < pNuevosDatosTrain->nNumEntradas; j++)
			pNuevosDatosTrain->entradas[i][j] = pDatosTrain->entradas[idx][j];

		for(int j = 0; j < pNuevosDatosTrain->nNumSalidas; j++)
			pNuevosDatosTrain->salidas[i][j] = pDatosTrain->salidas[idx][j];
	}

	for(int i = 0; i < pDatosValidacion->nNumPatrones; i++) // Train
	{
		int idx = indicesValidacion[i];

		for(int j = 0; j < pDatosValidacion->nNumEntradas; j++)
			pDatosValidacion->entradas[i][j] = pDatosTrain->entradas[idx][j];

		for(int j = 0; j < pDatosValidacion->nNumSalidas; j++)
			pDatosValidacion->salidas[i][j] = pDatosTrain->salidas[idx][j];
	}

	delete[] indicesTrain;

}

void PerceptronMulticapa::imprimirMatrizConfusion(Datos * pDatosTest, double ** prediccion)
{
	double ** m = new double*[pDatosTest->nNumSalidas];
	for(int i = 0; i < pDatosTest->nNumSalidas; i++)
	{
		m[i] = new double[pDatosTest->nNumSalidas];

		for(int j = 0; j < pDatosTest->nNumSalidas; j++)
			m[i][j] = 0;
	}

	for(int i = 0; i < pDatosTest->nNumPatrones; i++)
	{
		m[argMax(pDatosTest->salidas[i], pDatosTest->nNumSalidas)][argMax(prediccion[i], pDatosTest->nNumSalidas)] += 1;
	}

	for(int i = 0; i < pDatosTest->nNumSalidas; i++)
	{
		for(int j = 0; j < pDatosTest->nNumSalidas; j++)
				cout << m[i][j] << "\t";
		cout << endl;
	}


	for(int i = 0; i < pDatosTest->nNumSalidas; i++)
		delete[] m[i];

	delete[] m;

}


