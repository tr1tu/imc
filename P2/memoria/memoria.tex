\documentclass[12pt, a4paper, oneside, titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish, es-tabla]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage{ragged2e}
\usepackage[]{tocbibind}
\usepackage{float}
\usepackage{placeins}
\usepackage{graphics}


\author{Víctor Manuel Vargas Yun}
\title{Introducción a los modelos computacionales: Práctica 2}
\date{Curso académico 2017-2018\\Córdoba, \today}

\newcommand{\myparagraph}[1]{\paragraph{#1}\mbox{}\\}
\newcommand{\mysubparagraph}[1]{\subparagraph{#1}\mbox{}\\}

\renewcommand{\listalgorithmname}{Índice de algoritmos}

\begin{document}
\begin{titlepage}
\centering
\includegraphics[width=0.20\textwidth]{uco.png}
\includegraphics[width=0.18\textwidth]{eps.png}\par\vspace{1cm}
{\scshape\LARGE Universidad de Córdoba\par}
{\scshape\Large Escuela Politécnica Superior de Córdoba\par}
\vspace{1cm}
{\scshape\LARGE Ingeniería Informática\par}
{\scshape\Large Especialidad: Computación\par}
{\scshape\Large Cuarto curso. Primer cuatrimestre\par}
\vspace{1.5cm}
{\scshape\LARGE Introducción a los modelos computacionales.\par}
\vspace{1.5cm}
{\huge\bfseries Práctica 2: Perceptrón multicapa para problemas de clasificación.\par}
\vspace{1.2cm}
{\Large\itshape Víctor Manuel Vargas Yun\par}
20620656Y\par
i42vayuv@uco.es\par
\vfill

{\large Curso académico 2017-2018\\Córdoba, \today\par}
\end{titlepage}

\pagenumbering{roman}

\tableofcontents
\newpage

\listoffigures
\newpage

\listoftables
\newpage

\listofalgorithms
\addcontentsline{toc}{section}{Índice de algoritmos}
\newpage

\clearpage
\pagenumbering{arabic}

\def\BState{\State\hskip-\ALG@thistlm}

\setlength{\parskip}{0.7em}

\section{Adaptaciones para clasificación}
Para poder utilizar el modelo de perceptrón multicapa para problemas de clasificación, ha sido necesario realizar varias adaptaciones. La forma más habitual para la salida en problemas de clasificación consiste en tener una salida por cada posible clase de nuestro problema. La salida que tenga el valor más alto será la clase en la que se ha clasificado el patrón. Esta forma de representación se conoce como \emph{1-de-J}.

Siendo esto así, interesa que estas salidas tengan un sentido probabilístico. Para ello, se introduce la función Softmax en las neuronas de la capa de salida, de forma que se consigue todas estas salidas estén comprendidas entre 0 y 1 y además la suma de todas ellas sea 1.

Por otro lado, al tratarse de salidas probabilísticas, la función de MSE ya no es la función natural de error que se debe utilizar. Para estos casos es mejor utilizar función de error basada en entropía cruzada, que penaliza en mayor medida los errores cometidos sobre la clase correcta que sobre la incorrecta.

Por tanto, las adaptaciones realizadas se resumen en:

\begin{itemize}
	\item Implementar la función softmax para las neuronas de la capa de salida.
	
	Esto implica realizar cambios en la propagación de las entradas y en la retropropagación del error.
	\item Implementar la función de error basada en entropía cruzada.
	
	Implica realizar cambios en la función que calcula el error y en la retropropagación del error.
\end{itemize}

Además, como elemento añadido, independiente de las adaptaciones realizadas para clasificación, se ha añadido la posibilidad de realizar un entrenamiento \emph{offline} además de poder hacerlo \emph{online}. Para ello, ha sido necesario modificar la función de entrenamiento (cambios en la inicialización de los deltas y en la actualización de pesos).

\section{Funciones modificadas}
A continuación se describirá y se mostrará el pseudocódigo de aquellas funciones que hayan sido modificadas, ya sea para adaptarlas a problemas de clasificación o para implementar el modo \emph{offline}.

En primer lugar, el algoritmo \ref{propagarEntradas} muestra la nuevo función de propagar entradas. La adaptaciones se deben a la introducción de la capa de salida \emph{softmax}. Es necesario comprobar de qué tipo de capa se trata ya que la función de activación es diferente.

\begin{algorithm}[ht]
	\caption{Propagar entradas}\label{propagarEntradas}
	\begin{algorithmic}[1]
		\Procedure{propagarEntradas}{}
		\ForAll{capa $i$}
		\State $nets \gets [ ]$
		\State $netSum \gets 0.0$
		\ForAll{neurona $j$ de la capa $i$}
		\State $nets[j] \gets 0.0$
		\ForAll{neurona $k$ de la capa $i-1$}
		\State $nets[j] \gets nets[j] + x^{i-1}_k \times w^i_{jk}$
		\EndFor
		\State $nets[j] \gets nets[j] + w^i_{jn_{i-1}}$
		\State $netSum \gets netSum + exp(nets[j])$
		\EndFor
		\ForAll{neurona $j$ de la capa $i$}
		\If {tipo de capa $i$ es sigmoide}
			\State $x^i_j \gets \frac{1}{1+e^{-nets[j]}}$
		\Else
			\State $x^i_j \gets \frac{exp(nets[j])}{netSum}$
		\EndIf
		\EndFor
		\EndFor
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

La siguiente función que ha sido modificada ha sido la de retropropagar el error. Esta función, que se muestra en el algoritmo \ref{retropropagarError}, se ha visto afectada por la introducción de la capa \emph{softmax} así como por la introducción de la nueva función de error basada en la entropía cruzada.

\begin{algorithm}[ht]
	\caption{Retropropagar error}\label{retropropagarError}
	\begin{algorithmic}[1]
		\Procedure{retropropagarError}{objetivo, funcionError}
		\If{tipo de la capa $H-1$ es sigmoide}
			\ForAll{neurona $j$ de la capa de salida}
				\State $out \gets x^{H-1}_j$
				\State $\delta^{H-1}_j \gets - (objetivo_j - out ) \times out \times (1 - out)$
			\EndFor
		\Else
			\ForAll{neurona $j$ de la capa de salida}
				\State $out_j \gets x^{H-1}_j$
				\State $sum \gets 0.0$
				\ForAll{neurona $i$ de la capa de salida}
					\State $out_i \gets x^{H-1}_i$
					\If{$funcionError == mse$}
						\State $sum \gets sum + (objetivo_i - out_i) \times out_j \times (I(i==j) - out_i)$ 
					\Else
						\State $sum \gets sum + (objetivo_i / out_i) \times out_j \times (I(i==j) - out_i)$				
					\EndIf
				\EndFor
			\EndFor
		\EndIf
			
		\For{$i \gets H-2$ hasta $1$}
			\ForAll{neurona $j$ de la capa $i$}
				\State $s \gets 0.0$
				\State $out \gets x^i_j$
				\ForAll{neurona $k$ de la capa $i+1$}
					\State $s \gets s + w^{i+1}_{kj} \times \delta^{i+1}_k$
				\EndFor
				\State $\delta^i_j \gets s \times out \times (1 - out)$
			\EndFor
		\EndFor
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

Es necesario comprobar el tipo de función de error y el tipo de función de activación para poder calcular las derivadas ya que su cálculo es diferente dependiendo del valor de estas.

La siguiente función que ha sido necesario adaptar es la función que calcula el error de salida. Los cambios, que se muestran en el algoritmo \ref{calcularErrorSalida}, se deben a la incorporación de la función de error basada en entropía cruzada.

\begin{algorithm}[ht]
	\caption{Calcular error de salida}\label{calcularErrorSalida}
	\begin{algorithmic}[1]
		\Procedure{calcularErrorSalida}{target, funcionError}
			\State $error \gets 0.0$
			\If{$funcionError == entropiaCruzada$}
				\ForAll{neurona $i$ de la capa $H-1$}
					\State $error \gets error - target[i] \times \ln x^{H-1}_i$
				\EndFor
			\Else
				\ForAll{neurona $i$ de la capa $H-1$}
					\State $error \gets error + (target[i] - x^{H-1}_i)^2$
				\EndFor	
			\EndIf
			\State $error \gets \frac{error}{n^{H-1}}$
			\State devolver error
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\FloatBarrier

A continuación, se pasará a analizar las modificaciones realizadas debido a la incorporación del modo de entrenamiento \emph{offline}.

La primera función modificada ha sido la de simular red. En ella se debe comprobar si el entrenamiento es \emph{online} antes de realizar la inicialización de los deltas y la actualización de los pesos. Su nuevo pseudocódigo se muestra en el algoritmo \ref{simularRed}.

\begin{algorithm}[ht]
	\caption{Simular red}\label{simularRed}
	\begin{algorithmic}[1]
		\Procedure{simularRed}{entrada, objetivo, funcionError}
		\If{online}
			\ForAll{capa $h$, neurona $j$ de $h$ y neurona $i$ de $h-1$}
				\State $\Delta(t-1)_{ji}^h \gets \Delta_{ji}^h$
				\State $\Delta_{ji}^h \gets 0$
			\EndFor
		\EndIf
		
		\State alimentarEntradas(entrada)
		\State propagarEntradas()
		\State retropropagarError(objetivo, funcionError)
		\State acumularCambio()
		
		\If{online}
			\State ajustarPesos()
		\EndIf
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

La última función que ha sido modificada, también debido a la incorporación del modo \emph{offline}, es la de entrenar. Esta función, que se muestra en el algoritmo \ref{entrenar}, debe encargarse de inicializar los deltas y ajustar los pesos en el caso de que sea \emph{offline}.

\begin{algorithm}[ht]
	\caption{Entrenar}\label{entrenar}
	\begin{algorithmic}[1]
		\Procedure{entrenar}{pDatosTrain, funcionError}
		\If{offline}
		\ForAll{capa $h$, neurona $j$ de $h$ y neurona $i$ de $h-1$}
		\State $\Delta(t-1)_{ji}^h \gets \Delta_{ji}^h$
		\State $\Delta_{ji}^h \gets 0$
		\EndFor
		\EndIf
		
		\ForAll{patrón $i$ en pDatosTrain}
			\State simularRed(pDatosTrain[pDatosTrain.entradas[i], pDatosTrain.salidas[i]], funcionError)
		\EndFor
		
		\If{offline}
			\ForAll{capa oculta o de salida $i$}
				\State $\eta_i \gets F^{-(H-i)} \times \eta$
				\ForAll{neurona $j$ de la capa $i$}
					\ForAll{neurona $k$ de la capa $i-1$}
						\State $w^i_{jk} \gets w^i_{jk} - \frac{\eta_i \times \Delta^i_{jk}(t)}{N} - \frac{\mu \times \eta_i \times \Delta^i_{jk}(t-1)}{N}$
					\EndFor
					
					\State $w^i_{jn_{H-1}} \gets w^i_{jn_{H-1}} - \frac{\eta_i \times \Delta^i_{jn_{H-1}}(t)}{N} - \frac{\mu \times \eta_i \times \Delta^i_{jn_{H-1}}(t-1)}{N}$
				\EndFor
			\EndFor
		\EndIf
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\FloatBarrier

\section{Experimentos y análisis de resultados}
\subsection{Experimento 1: Mejor arquitectura de red}
El primer experimento consistía en buscar la mejor arquitectura para las bases de datos \emph{iris} y \emph{noMNIST}. Para la base de datos \emph{xor} se utiliza la mejor arquitectura encontrada en la práctica anterior.

El experimento consiste en probar 8 arquitecturas diferentes: 1 o 2 capas con 5, 10, 50 o 75 neuronas cada una. Los resultados obtenidos se pueden observar en la Tabla \ref{table:Experimento1}. En ella se pueden observar los valores de CCR para \emph{train} y para \emph{test} y el número medio de iteraciones realizadas.

El límite de iteraciones se ha fijado en 1000 para \emph{xor} e \emph{iris} y 500 para \emph{noMNIST}. Se ha utilizado un $0.1$ de validación y se ha desactivado el factor de decremento. Para los parámetros $\eta$ y $\mu$ se han usado los valores $0.7$ y $1$. Como función de error se ha utilizado la función de error basada en entropía cruzada y para la capa de salida se ha utilizado la función de activación \emph{softmax}.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{c | c | c | c | c | c | c | c}
		Dataset & \emph{l} & \emph{h} & $\overline{CCR}$ Train & $\sigma_{CCR}$ Train & $\overline{CCR}$ Test & $\sigma_{CCR}$ Test & Nº It.\\\hline
		iris & 1 & 5 & $98.4158$ & $0.792079$ & $94.7368$ & $0$ & $1000$\\
		iris & 1 & 10 & $97.8218$ & $0.740922$ & $96.3158$ & $2.10526$ & $894.8$\\
		iris & 1 & 50 & $98.2178$ & $0.970095$ & $94.7368$ & $0$ & $952.8$\\
		iris & 1 & 75 & $98.4158$ & $0.485047$ & $96.3158$ & $2.10526$ & $1000$\\
		iris & 2 & 5 & $98.0198$ & $0$ & $94.7368$ & $0$ & $1000$\\
		iris & 2 & 10 & $98.2178$ & $0.39604$ & $94.7368$ & $0$ & $982.8$\\
		iris & 2 & 50 & $98.4158$ & $0.792079$ & $94.7368$ & $0$ & $969$\\
		iris & 2 & 75 & $98.2178$ & $0.39604$ & $96.8421$ & $2.57841$ & $830.6$\\
		noMNIST & 1 & 5 & $93.3333$ & $0.331269$ & $82.4$ & $3.2619$ & $500$\\
		noMNIST & 1 & 10 & $96.716$ & $0.354388$ & $82.9333$ & $3.63563$ & $500$\\
		noMNIST & 1 & 50 & $100$ & $0$ & $83.6667$ & $1.05409$ & $500$\\
		noMNIST & 1 & 75 & $100$ & $0$ & $83.3333$ & $1.53478$ & $500$\\
		noMNIST & 2 & 5 & $92.0741$ & $1.05453$ & $81.1333$ & $2.12498$ & $500$\\
		noMNIST & 2 & 10 & $97.1358$ & $0.285823$ & $82.8667$ & $2.09338$ & $500$\\
		noMNIST & 2 & 50 & $100$ & $0$ & $84.6$ & $1.65193$ & $500$\\
		noMNIST & 2 & 75 & $100$ & $0$ & $84.3333$ & $1.3499$ & $500$\\
		xor & 2 & 100 & $100$ & $0$ & $100$ & $0$ & $554.4$\\
		
	\end{tabular}
	}
	\caption{Resultados del experimento 1}
	\label{table:Experimento1}
\end{table}

Las mejores arquitecturas obtenidas han sido:
\begin{itemize}
	\item \emph{XOR}: 2 capas de 100 neuronas.
	\item \emph{iris}: 2 capas de 75 neuronas.
	\item \emph{noMNIST}: 2 capas de 50 neuronas.
\end{itemize}

Para obtener estas conclusiones se ha priorizado el CCR de test, ya que lo que realmente se busca es que el modelo sea capaz de generalizar.

\FloatBarrier

\subsection{Experimento 2: Función de error y de activación}
En el segundo experimento se ha tratado de determinar cuál es la mejor función de error para cada base de datos y cuál es la mejor función de activación para las neuronas de la capa de salida. De nuevo se han vuelto a utilizar los mismos parámetros que se comentaron para el experimento 1 y, además, se ha utilizado la mejor arquitectura encontrada en el experimento anterior.

Los resultados de este experimento se pueden observar en la Tabla \ref{table:Experimento2}.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{c | c | c | c | c | c | c}
		Dataset & \emph{F.E.} & \emph{F.A.} & $\overline{CCR}$ Train & $\sigma_{CCR}$ Train & $\overline{CCR}$ Test & $\sigma_{CCR}$ Test\\\hline	
		iris & MSE & Sig & $97.3214$ & $0$ & $94.7368$ & $0$\\
		iris & MSE & Sof & $72.3214$ & $32.0783$ & $70.5263$ & $29.6517$\\
		iris & ENT & Sof & $98.2143$ & $0$ & $94.7368$ & $0$\\
		noMNIST & MSE & Sig & $96.1556$ & $0.577778$ & $84.5333$ & $0.956847$\\
		noMNIST & MSE & Sof & $97.6$ & $0.348542$ & $83.4$ & $0.827312$\\
		noMNIST & ENT & Sof & $100$ & $0$ & $84.2$ & $1.06667$\\
		xor & MSE & Sig & $100$ & $0$ & $100$ & $0$\\
		xor & MSE & Sof & $50$ & $0$ & $50$ & $0$\\
		xor & ENT & Sof & $100$ & $0$ & $100$ & $0$\\
	\end{tabular}
	}
	\caption{Resultados del experimento 2}
	\label{table:Experimento2}
\end{table}

Las mejores resultados se han obtenido con las siguientes funciones de error:
\begin{itemize}
	\item \emph{XOR}: Entropía cruzada y softmax.
	\item \emph{iris}: Entropía cruzada y softmax.
	\item \emph{noMNIST}: Entropía cruzada y softmax.
\end{itemize}

Tiene sentido que los tres conjuntos de datos hayan tenido mejores resultados con la función de entropía cruzada y la función \emph{softmax} ya que son funciones que están pensadas para \textbf{clasificación} mientras que el MSE y la función sigmoide para la capa de salida están pensadas para \textbf{regresión}.

La combinación de \textbf{entropía cruzada} y \textbf{sigmoide} no se ha probado ya que se sabe que va a ofrecer malos resultados ya que la función de entropía cruzada necesita que las salidas sean probabilísticas \cite{Goodfellow-et-al-2016} y si se usa una sigmoide en la capa de salida, no lo serán. Para conseguir que las salidas representen probabilidades, es necesario utilizar la función \emph{softmax} en la salida.

\FloatBarrier

\subsection{Experimento 3: Online VS Offline}
En este tercer experimento se tratará de determinar qué modo de entrenamiento proporciona mejores resultados. Para ello, se utilizarán los parámetros que se comentaron en el experimento 1, la mejor arquitectura obtenida durante dicho experimento y las funciones de error y activación obtenidas durante el experimento 2.

Los resultados obtenidos se pueden observar en la Tabla \ref{table:Experimento3}.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{c | c | c | c | c | c | c}
		Dataset & \emph{online} & $\overline{CCR}$ Train & $\sigma_{CCR}$ Train & $\overline{CCR}$ Test & $\sigma_{CCR}$ Test & Nº It.\\\hline	
		iris & 0 & $98.2178$ & $0.39604$ & $96.8421$ & $2.57841$ & $830.6$\\
		iris & 1 & $99.0099$ & $1.25239$ & $95.7895$ & $2.10526$ & $593.4$\\
		noMNIST & 0 & $100$ & $0$ & $84.6$ & $1.65193$ & $500$\\
		noMNIST & 1 & $69.358$ & $9.75337$ & $63.0667$ & $7.44879$ & $500$\\
		xor & 0 & $100$ & $0$ & $100$ & $0$ & $554.4$\\
		xor & 1 & $100$ & $0$ & $100$ & $0$ & $279.2$\\
		
	\end{tabular}
	}
	\caption{Resultados del experimento 3}
	\label{table:Experimento3}
\end{table}

En este caso, los mejores resultados se han obtenido con los siguientes modos:
\begin{itemize}
	\item \emph{XOR}: online (tiene el mismo CCR que el offline pero ha necesitado menos iteraciones).
	\item \emph{iris}: offline.
	\item \emph{noMNIST}: offline.
\end{itemize}

Al parecer, el método \emph{online} funciona bien cuando la base de datos tiene pocos patrones. Cuando se trata de bases de datos con más patrones, el entrenamiento \emph{offline} ha demostrado ser mejor.

\FloatBarrier

\subsection{Experimento 4: Parámetros $v$ y $d$}
Por último, se tratará de buscar los mejores valores para los parámetros $v$ y $d$ que determinan, respectivamente, el porcentaje de patrones de \emph{train} que se usarán para validación y el factor de decremento. La base de datos \emph{XOR} no puede utilizar validación ya que solo cuenta con cuatro patrones.

De nuevo, se han vuelto a utilizar los mismos parámetros y se han actualizado con las mejores configuraciones obtenidas en los experimentos anteriores.

También era de interés comprobar si al utilizar conjunto de validación se reducía el número medio de iteraciones necesarias para entrenar. Con este propósito, se ha calculado también el número medio de iteraciones.

Los resultados obtenidos se muestran en la tabla \ref{table:Experimento4}.

\begin{table}[!htbp]
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{c | c | c | c | c | c | c | c}
		Dataset & \emph{v} & \emph{d} & $\overline{CCR}$ Train & $\sigma_{CCR}$ Train & $\overline{CCR}$ Test & $\sigma_{CCR}$ Test & Nº It.\\\hline	
		iris & 0 & 1 & $98.2143$ & $0$ & $94.7368$ & $0$ & $1000$\\
		iris & 0 & 2 & $98.0357$ & $0.357143$ & $94.7368$ & $0$ & $1000$\\
		iris & 0.1 & 1 & $98.2178$ & $0.39604$ & $96.8421$ & $2.57841$ & $830.6$\\
		iris & 0.1 & 2 & $98.0198$ & $0.626194$ & $96.8421$ & $2.57841$ & $1000$\\
		iris & 0.2 & 1 & $98.4444$ & $0.888889$ & $97.3684$ & $2.35376$ & $1000$\\
		iris & 0.2 & 2 & $98.4444$ & $0.888889$ & $96.8421$ & $2.57841$ & $1000$\\
		noMNIST & 0 & 1 & $100$ & $0$ & $84.2$ & $1.06667$ & $500$\\
		noMNIST & 0 & 2 & $99.6889$ & $0.129577$ & $81.9333$ & $0.533333$ & $500$\\
		noMNIST & 0.1 & 1 & $100$ & $0$ & $84.6$ & $1.65193$ & $500$\\
		noMNIST & 0.1 & 2 & $99.8025$ & $0.148148$ & $81.4667$ & $0.884433$ & $500$\\
		noMNIST & 0.2 & 1 & $100$ & $0$ & $84.6$ & $1.23648$ & $500$\\
		noMNIST & 0.2 & 2 & $99.9722$ & $0.0555556$ & $81.9333$ & $1.37275$ & $500$\\
		xor & 0 & 1 & $100$ & $0$ & $100$ & $0$ & $279.2$\\
		xor & 0 & 2 & $100$ & $0$ & $100$ & $0$ & $297$\\	
	\end{tabular}
	}
	\caption{Resultados del experimento 4}
	\label{table:Experimento4}
\end{table}


Los mejores valores de los parámetros para cada base de datos han sido:
\begin{itemize}
	\item \emph{XOR}: $v = 0.0, F = 1$ (mismo CCR pero menos iteraciones).
	\item \emph{iris}: $v = 0.2, F = 1$.
	\item \emph{noMNIST}: $v = 0.2, F = 1$.
\end{itemize}

En cuanto al número de iteraciones, se puede comprobar que en un caso de \emph{iris}, el número de iteraciones empleado se ha reducido al utilizar conjunto de validación. Sin embargo, en el resto, no ha tenido ningún efecto sobre el número de iteraciones.

También es cierto que al utilizar validación, el número de patrones de entrenamiento es menor y, por lo tanto, se reduce el número de patrones que deben alimentarse en la red. Esto no supone una reducción del número de iteraciones pero sí que puede suponer una reducción de tiempo de entrenamiento (tal y como se comprobó en la práctica anterior).

\FloatBarrier

\subsection{Resumen de los experimentos}
A continuación se resumen las mejores configuraciones que se han obtenido para cada conjunto de datos:
\begin{itemize}
	\item \emph{XOR}: 2 capas de 100 neuronas, entropía cruzada y \emph{softmax}, modo online, $v = 0.0$ y $F = 1$.
	\item \emph{iris}: 2 capas de 75 neuronas, entropía cruzada y \emph{softmax}, modo offline, $v = 0.2$ y $F = 1$.
	\item \emph{noMNIST}: 2 capas de 50 neuronas, entropía cruzada y \emph{softmax}, modo offline, $v = 0.2$ y $F = 1$.
\end{itemize}

La Tabla \ref{table:MejoresCCR} muestra los mejores resultados de CCR obtenidos para cada base de datos.

\begin{table}[!htbp]
	\centering
		\begin{tabular}{l | c | c }
			Dataset & CCR Train & CCR Test\\\hline
			\emph{XOR} & $100$ & $100$\\
			\emph{iris} & $98.4444$ & $97.3684$\\
			\emph{noMNIST} & $100$ & $84.5333$
		\end{tabular}
	\caption{Mejores resultados para cada conjunto de datos}
	\label{table:MejoresCCR}
\end{table}

Las bases de datos XOR e iris han utilizado la mayor arquitectura de red que se ha probado. Sin embargo, en el caso de \emph{noMNIST}, se ha utilizado la de 2 capas de 50 neuronas, que ha dado los mismos resultados en train que 2 capas de 75 neuronas pero, sin embargo, en test ha dado mejores resultados. Esto se puede deber a que la arquitectura más grande había sobreaprendido más y por tanto tenía una peor capacidad de generalización. Este sobreajuste se puede deber a que algunas de las imágenes del conjunto de entrenamiento pueden tener ruido o pueden ser formas poco descriptivas de la clase a la que pertenecen.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{2.png}
	\includegraphics[scale=1]{3.png}
	\includegraphics[scale=1]{5.png}
	\includegraphics[scale=1]{6.png}
	\includegraphics[scale=1]{7.png}
	\includegraphics[scale=1]{12.png}
	\includegraphics[scale=1]{41.png}
	\includegraphics[scale=1]{149.png}
	\includegraphics[scale=1]{189.png}
	\includegraphics[scale=1]{201.png}
	\includegraphics[scale=1]{397.png}
	\includegraphics[scale=1]{537.png}
	\includegraphics[scale=1]{608.png}
	\includegraphics[scale=1]{852.png}
	\caption{Ejemplos de ruido en \emph{Train} de \emph{noMNIST}}
	\label{fig:RuidoTrain}
\end{figure}


En la Figura \ref{fig:RuidoTrain} se pueden observar una serie de patrones de \emph{train} que no representan ninguna letra, representan una letra de forma confusa o representan una letra incorrecta.
Estos patrones son minoritarios en el conjunto de entrenamiento. Sin embargo, si el modelo sobreajusta, llegará a aprender incluso estos patrones que no son otra cosa que ruido. Por otra parte, un modelo con muchas capas y neuronas tiene mayor tendencia a sobreentrenar. Por ello, la arquitectura 75,75 ha funcionado peor que la 50,50 en \emph{noMNIST}.


\subsection{Matriz de confusión para \emph{noMNIST}}

La Tabla \ref{table:MatrizConfusiónNomnist} muestra la matriz de confusión de la mejor configuración obtenida con \emph{noMNIST}. La clase real está representada en la parte izquierda y la clase predicha en la parte superior.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c c c c c c c}
		&A&B&C&D&E&F\\
		A&46&1&0&2&0&1\\
		B&1&42&1&1&3&2\\
		C&1&1&44&0&2&2\\
		D&2&3&2&41&2&0\\
		E&3&0&1&0&41&5\\
		F&3&0&0&1&2&44
	\end{tabular}
	\caption{Matriz de confusión de la mejor configuración \emph{noMNIST}}
	\label{table:MatrizConfusiónNomnist}
\end{table}

Si se observa la matriz de confusión, se puede ver que el mayor número de errores se comete al clasificar las clases E y F. La E se ha clasificado 5 veces como F y la F se ha clasificado 2 veces como E. En total, suman 7 errores que es la cifra más alta. Esto era un resultado que se esperaba ya que las letras E y F son las que tienen mayor parecido.

\subsection{Ejemplos de erores en \emph{noMNIST}}

A continuación, se mostrarán algunos ejemplos de patrones que se han clasificado mal para poder comprobar si realmente las imágenes son confusas o si se trata de un error del clasificador.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{A8_F.png}
	\includegraphics[scale=1]{A17_D.png}
	\includegraphics[scale=1]{A29_B.png}
	\caption{Patrones de A mal clasificados}
	\label{fig:PatronesMalA}
\end{figure}

La Figura \ref{fig:PatronesMalA} muestra algunos de los patrones de A que se han clasificado mal. El primer patrón ha sido clasificado como F, el segundo como D y el tercero como B. Para una persona sería fácil distinguir que las letras de las imágenes pertenecen a la clase A. Es muy probable que los errores en la clasificación de estos patrones se deban a que la fuente que utilizan es algo compleja.

\FloatBarrier

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{B65_F.png}
	\includegraphics[scale=1]{B73_D.png}
	\includegraphics[scale=1]{B79_A.png}
	\caption{Patrones de B mal clasificados}
	\label{fig:PatronesMalB}
\end{figure}

La Figura \ref{fig:PatronesMalB} muestra algunos de los patrones de B que se han clasificado mal. El primer patrón ha sido clasificado como F, el segundo como D y el tercero como A. Los errores en este caso se pueden deber a lo mismo que en el caso de la A.

\FloatBarrier

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{C108_A.png}
	\includegraphics[scale=1]{C149_B.png}
	\caption{Patrones de C mal clasificados}
	\label{fig:PatronesMalC}
\end{figure}

La Figura \ref{fig:PatronesMalC} muestra algunos de los patrones de C que se han clasificado mal. El primer patrón ha sido clasificado como A y el segundo como B. En este caso, las letras se distinguen bastante bien y por tanto se puede considerar como errores del clasificador.

\FloatBarrier

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{D157_C.png}
	\includegraphics[scale=1]{D177_E.png}
	\caption{Patrones de D mal clasificados}
	\label{fig:PatronesMalD}
\end{figure}

La Figura \ref{fig:PatronesMalD} muestra algunos de los patrones de D que se han clasificado mal. El primer patrón ha sido clasificado como C y el segundo como E. De nuevo, las letras se pueden distinguir bastante bien y se puede considerar un error del modelo.

\FloatBarrier

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{E207_A.png}
	\includegraphics[scale=1]{E208_F.png}
	\caption{Patrones de E mal clasificados}
	\label{fig:PatronesMalE}
\end{figure}

La Figura \ref{fig:PatronesMalE} muestra algunos de los patrones de E que se han clasificado mal. El primer patrón ha sido clasificado como A y el segundo como F. En este caso tampoco debería tener mucho problema para clasificar.

\FloatBarrier

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=1]{F261_A.png}
	\includegraphics[scale=1]{F282_E.png}
	\caption{Patrones de F mal clasificados}
	\label{fig:PatronesMalF}
\end{figure}

La Figura \ref{fig:PatronesMalF} muestra algunos de los patrones de F que se han clasificado mal. El primer patrón ha sido clasificado como A y el segundo como E. La segunda imagen es fácilmente confundible con una E ya que son bastante similares.

\subsection{Gráficas de convergencia}

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=0.7]{xor_ccr_vs_it.png}
	\caption{Gráfica de convergencia de XOR}
	\label{fig:XORCCRvsIT}
\end{figure}

La Figura \ref{fig:XORCCRvsIT} muestra el CCR de entrenamiento/test frente al número de iteraciones realizadas en la ejecución del algoritmo de clasificación con el conjunto de datos xor utilizando la mejor configuración encontrada.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=0.6]{iris_ccr_vs_it.png}
	\caption{Gráfica de convergencia de iris}
	\label{fig:IrisCCRvsIT}
\end{figure}

La Figura \ref{fig:IrisCCRvsIT} muestra el CCR de entrenamiento/test frente al número de iteraciones realizadas en la ejecución del algoritmo de clasificación con el conjunto de datos iris utilizando la mejor configuración encontrada.

\begin{figure}[!htbp]
	\centering
	\includegraphics[scale=0.6]{nomnist_ccr_vs_it.png}
	\caption{Gráfica de convergencia de \emph{noMNIST}}
	\label{fig:nomnistCCRvsIT}
\end{figure}

La Figura \ref{fig:nomnistCCRvsIT} muestra el CCR de entrenamiento/test frente al número de iteraciones realizadas en la ejecución del algoritmo de clasificación con el conjunto de datos \emph{noMNIST} utilizando la mejor configuración encontrada.

Si se observan las tres gráficas, se puede apreciar como dependiendo de la complejidad de la base de datos, la subida del CCR es más rápida o más gradual.

\RaggedRight
\bibliographystyle{ieeetr}
\bibliography{memoria}

\end{document}